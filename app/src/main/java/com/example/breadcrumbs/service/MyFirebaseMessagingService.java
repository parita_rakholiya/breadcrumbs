package com.example.breadcrumbs.service;


import static com.example.breadcrumbs.activity.NavigationActivityKt.EXTRA_NAVIGATION_POINT;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.activity.HomeScreenActivity;
import com.example.breadcrumbs.activity.NavigationActivity;
import com.example.breadcrumbs.activity.TurnByTurnExperienceActivity;
import com.example.breadcrumbs.model.GetMyRoutesModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.PrefManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String NOTIFICATION_CHANNEL_ID = "BreadCrumbs";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getNotification() != null) {
            Log.d("TAG", "Message From " + remoteMessage.getFrom());
            Log.d("TAG", "data From " + remoteMessage.getData());
            Log.d("TAG", "Notification Title " + remoteMessage.getNotification().getTitle());
            Log.d("TAG", "Notification Body " + remoteMessage.getNotification().getBody());

            Gson gson = new Gson();
            Type type = new TypeToken<GetMyRoutesModel.Data>() {
            }.getType();
            JSONObject object = new JSONObject(remoteMessage.getData());
            GetMyRoutesModel.Data data = null;
            try {
                data = gson.fromJson(object.get("message").toString(), type);
                showNotification(getApplicationContext(), remoteMessage.getNotification().getBody(),data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @SuppressLint("WrongConstant")
    public static void showNotification(Context activity, String message, GetMyRoutesModel.Data data) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(activity);
        mBuilder.setSmallIcon(R.drawable.app_icon);
        mBuilder.setContentTitle(activity.getString(R.string.app_name));
        mBuilder.setContentText(message);
        Bitmap icon = BitmapFactory.decodeResource(activity.getResources(), R.drawable.app_icon);
        mBuilder.setLargeIcon(icon);
        mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);

        if (Build.VERSION.SDK_INT> Build.VERSION_CODES.KITKAT)
        {
            mBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        }
        NotificationManager mNotificationManager = (NotificationManager)activity.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(false);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;

        if(isAppIsInBackground(activity)){
            Intent resultIntent = new Intent(activity, HomeScreenActivity.class);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            resultIntent.setFlags(Notification.FLAG_AUTO_CANCEL);
            resultIntent.putExtra(Constants.ROUTE_DETAIL,data);
            PendingIntent intent = PendingIntent.getActivity(activity, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(intent);
        }else {
            Intent resultIntent = new Intent(activity, NavigationActivity.class);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            resultIntent.setFlags(Notification.FLAG_AUTO_CANCEL);
            resultIntent.putExtra(Constants.ROUTE_DETAIL,data);
            PendingIntent intent = PendingIntent.getActivity(activity, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(intent);
        }

        mNotificationManager.notify(1, mBuilder.build());

    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        }else{
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        Log.d("background===",""+isInBackground);
        return isInBackground;
    }

}
