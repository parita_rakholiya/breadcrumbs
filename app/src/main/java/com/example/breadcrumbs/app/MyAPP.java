package com.example.breadcrumbs.app;

import android.app.Application;
import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.multidex.MultiDex;

import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.PrefManager;
import com.example.breadcrumbs.utils.ViewControll;
import com.onesignal.OneSignal;


public class MyAPP extends Application {

    private static final String ONESIGNAL_APP_ID = "154287d1-18fc-4576-8b2e-91288048c3ce";
    private static Context context;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onCreate() {
        super.onCreate();

        MyAPP.context = getApplicationContext();
        MultiDex.install(context);
        // Enable verbose OneSignal logging to debug issues if needed.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

        // OneSignal Initialization
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);
    }

    public static Context getAppContext() {


        return MyAPP.context;
    }

    @Override
    public void attachBaseContext(Context base) {

        super.attachBaseContext(base);

    }

}
