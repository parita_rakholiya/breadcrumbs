package com.example.breadcrumbs.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.text.LocaleKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.activity.HomeScreenActivity;
import com.example.breadcrumbs.activity.LoginScreenActivity;
import com.example.breadcrumbs.adapter.DistanceListAdapter;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.fragment.MyProfileFragment;
import com.example.breadcrumbs.fragment.MyRoutesFragment;
import com.example.breadcrumbs.interfaces.saveRouteClickListener;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.AddPathWithPin;
import com.example.breadcrumbs.model.CommonResponseModel;
import com.example.breadcrumbs.model.CountryData;
import com.example.breadcrumbs.model.DistanceListModel;
import com.example.breadcrumbs.model.GetDistanceModel;
import com.example.breadcrumbs.model.GetMyRoutesModel;
import com.example.breadcrumbs.model.LockedUserModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapbox.mapboxsdk.geometry.LatLng;


import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;
import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ir.shahabazimi.instagrampicker.interfaces.dialogPositiveClickListener;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ViewControll {

    private static final String TAG = "ViewControll" ;
    private  static String addSource = "";
    private  static String addDestination = "";
    private  static LatLng startingPointLatLng = null;

    public static RequestBody convertStringToRequestBody(String itemValue){
        return RequestBody.create(itemValue, MediaType.parse("text/plain"));
    }

    public static boolean isStoragePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    public static void showAlertDialog(Activity activity, String title, String message, dialogPositiveClickListener dialogPositiveClickListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.CustomAlertDialog);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogPositiveClickListener.positiveClick();
                dialog.dismiss();

            }
        });
        builder.setNegativeButton(activity.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogPositiveClickListener.negativeClick();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void showLockedAlertDialog(Activity activity, String title, String message, dialogPositiveClickListener dialogPositiveClickListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.CustomAlertDialog);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogPositiveClickListener.positiveClick();
                dialog.dismiss();

            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    public static boolean isLocationPermissionGranted(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        Constants.LOCATIONS);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        Constants.LOCATIONS);
            }
            return false;
        } else {
            return true;
        }
    }

    public static String getSelectedLanguage(){
       String language = LanguagePrefManager.getString(Constants.SELECTED_LANGUAGE,"en");
       if(language.equals("en")){
           return "English";
       }else {
           return "Arabic";
       }
    }

    public static void loadFragment(Activity activity, Fragment fragment, String tag, List<Fragment> fragmentList) {
        ViewControll.checkLockedUserApi(activity);
        int index = getFragmentIndex(fragment,fragmentList);
        FragmentTransaction transaction = ((AppCompatActivity)activity).getSupportFragmentManager().beginTransaction();
        if (fragment.isAdded()) {
            // if the fragment is already in container
            ((HomeScreenActivity)activity).homeScreenBinding.txtTitle.setText(tag);
            if(fragment instanceof MyProfileFragment){
                ((HomeScreenActivity)activity).homeScreenBinding.txtEdit.setVisibility(View.VISIBLE);
                ((HomeScreenActivity)activity).homeScreenBinding.imgBack.setVisibility(View.GONE);
                ((MyProfileFragment) fragment).backFromEdit();
            }else if(fragment instanceof MyRoutesFragment){
                ((HomeScreenActivity)activity).homeScreenBinding.txtEdit.setVisibility(View.GONE);
                ((MyRoutesFragment) fragment).backFromHistory();
            }else {
                ((HomeScreenActivity)activity).homeScreenBinding.txtEdit.setVisibility(View.GONE);
                ((HomeScreenActivity)activity).homeScreenBinding.imgBack.setVisibility(View.GONE);
            }
            Log.d("fragment==","show-"+fragment);
            transaction.show(fragment);

        } else { // fragment needs to be added to frame container

            transaction.add(R.id.nav_main_fragment, fragment,tag);
            ((HomeScreenActivity)activity).homeScreenBinding.txtTitle.setText(tag);
            if(fragment instanceof MyProfileFragment){
                ((HomeScreenActivity)activity).homeScreenBinding.txtEdit.setVisibility(View.VISIBLE);
                ((HomeScreenActivity)activity).homeScreenBinding.imgBack.setVisibility(View.GONE);
            }else {
                ((HomeScreenActivity)activity).homeScreenBinding.txtEdit.setVisibility(View.GONE);
                ((HomeScreenActivity)activity).homeScreenBinding.imgBack.setVisibility(View.GONE);
            }
            Log.d("fragment==","add-"+fragment);
        }
        // hiding the other fragments
        for (int i = 0; i < fragmentList.size(); i++) {
            if (fragmentList.get(i).isAdded() && i != index) {
                transaction.hide(fragmentList.get(i));
                Log.d("fragment==","hide-"+fragmentList.get(i));
            }
        }
        transaction.commit();
    }

    private static int getFragmentIndex(Fragment fragment, List<Fragment> fragmentList) {
        int index = -1;
        for (int i = 0; i < fragmentList.size(); i++) {
            if (fragment.hashCode() == fragmentList.get(i).hashCode()){
                return i;
            }
        }
        return index;
    }

    public static void hideKeyBoard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static void  showKeyBoard(Activity activity){
        InputMethodManager imm = (InputMethodManager)
                activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null){
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    public static void  getCountryList(Activity activity,AutoCompleteTextView autoCompleteTextView){
        ArrayList<String> countryList = new ArrayList<String>();
        Locale[] locale = Locale.getAvailableLocales();
        String country;
        for( CountryData data : getAllCountryInfo(activity) ){
          /*  country = loc.getDisplayCountry();
            if( country.length() > 0 && !countryList.contains(country) ){
                countryList.add( country );
            }*/
            countryList.add(data.getCountryName());
        }
        Collections.sort(countryList, String.CASE_INSENSITIVE_ORDER);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                R.layout.txt_row_country,R.id.txt_row, countryList);
        autoCompleteTextView.setAdapter(adapter);
        autoCompleteTextView.showDropDown();
    }

    public static void  setGenderList(Activity activity,AutoCompleteTextView autoCompleteTextView){
        ArrayList<String> genderList = new ArrayList<String>();
        genderList.add(activity.getString(R.string.male));
        genderList.add(activity.getString(R.string.female));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                R.layout.list_textview, genderList);
        autoCompleteTextView.setAdapter(adapter);
        autoCompleteTextView.showDropDown();
    }



    public static boolean isPasswordValidMethod(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^" +
                "(?=.*[0-9])" +         //at least 1 digit
                "(?=.*[a-z])" +         //at least 1 lower case letter
                "(?=.*[A-Z])" +         //at least 1 upper case letter
                "(?=.*[a-zA-Z])" +      //any letter
                "(?=.*[@#$%^&+=])" +    //at least 1 special character
                "(?=\\S+$)" +           //no white spaces
                ".{6,}" +               //at least 8 characters
                "$";
//        final String PASSWORD_PATTERNRD_PATTERN = "^(?=.*[A-Za-z])(?=.*\\\\d)(?=.*[$@$!%*#?&])[A-Za-z\\\\d$@$!%*#?&]{8,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public static String getAssetJsonData(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("country_phone_number_details");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        Log.e("data", json);
        return json;

    }

    public static List<CountryData> getAllCountryInfo(Activity activity){
        String data = getAssetJsonData(activity);
        List<CountryData> countryDataList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("countryList");
            for(int i=0;i<jsonArray.length();i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                String code = object.getString("code");
                String name = object.getString("label");
                String phone = object.getString("phone");
                Object length = object.get("phoneLength");
                List<Integer> phoneCodeLengthList = new ArrayList<>();
                if (length instanceof JSONArray) {
                    JSONArray lengthList = object.getJSONArray("phoneLength");
                    for (int j = 0; j < lengthList.length(); j++) {

                        int lenght = lengthList.getInt(j);
                        phoneCodeLengthList.add(lenght);
                    }
                }else {
                    phoneCodeLengthList.add(object.getInt("phoneLength"));
                }

                CountryData countryData = new CountryData();
                countryData.setCountryCode(code);
                countryData.setCountryName(name);
                countryData.setCountryPhoneCode(phone);
                countryData.setPhoneNolength(phoneCodeLengthList);
                countryDataList.add(countryData);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return countryDataList;

    }

    public static boolean checkPhoneNumberValidation(List<CountryData>countryDataList,
                                                     String country,String phoneNo){
        boolean isPhoneNumberValid = false;
        for (CountryData countryData:countryDataList){
            if(countryData.getCountryName().equalsIgnoreCase(country)){
                for(int i=0;i<countryData.getPhoneNolength().size();i++){
                    int length = countryData.getPhoneNolength().get(i);
                    if(phoneNo.length()==length){
                        isPhoneNumberValid =true;
                        break;
                    }
                }
            }
        }
        return isPhoneNumberValid;
    }

    public static PopupWindow openStartStopRouteDialog(Activity activity, String message, String isFrom, dialogPositiveClickListener clickListener){
        LayoutInflater layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popup_start_route,null);

        Button btnCancle = (Button) customView.findViewById(R.id.btn_cancle);
        Button btnStart = (Button) customView.findViewById(R.id.btn_start);
        TextView txtMsg = (TextView) customView.findViewById(R.id.txt_msg);
        txtMsg.setText(message);
        if(isFrom.equals(Constants.START)){
            btnStart.setText(activity.getString(R.string.start));
        }else if(isFrom.equals(Constants.STOP)){
            btnStart.setText(activity.getString(R.string.stop));
        }else {
            btnStart.setText(activity.getString(R.string.pin));
        }
        PopupWindow popupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
                clickListener.positiveClick();
            }
        });

        ViewGroup root = (ViewGroup)activity.getWindow().getDecorView().getRootView();
        popupWindow.setTouchable(true);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        popupWindow.setWidth(width-50);
        popupWindow.showAtLocation(customView, Gravity.CENTER, 0, 0);
        applyDim(root,0.6F);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                clearDim(root);
            }
        });
        return popupWindow;

    }

    public static void openDistanceListDailog(Activity activity, boolean isfromSetting, TextView textView, View layout, View viewLayout){
        LayoutInflater layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popup_distance,null);

        ListView listViewSort = (ListView) customView.findViewById(R.id.list_distance);
        ProgressBar progressBar = (ProgressBar) customView.findViewById(R.id.progress);
        ImageView imgCancle = (ImageView) customView.findViewById(R.id.img_cancle);
        int height = 500;
        if(!isfromSetting){
            height = ViewGroup.LayoutParams.MATCH_PARENT;
            imgCancle.setVisibility(View.VISIBLE);
        }else {
            imgCancle.setVisibility(View.GONE);
        }

        PopupWindow popupWindow = new PopupWindow(customView, height,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        progressBar.setVisibility(View.VISIBLE);

        APIManager.getDistanceList(new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                GetDistanceModel getDistanceModel = (GetDistanceModel) modelclass;
                progressBar.setVisibility(View.GONE);
                if(!getDistanceModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Toast.makeText(activity, getDistanceModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }

                ArrayList<DistanceListModel> sortList = new ArrayList<>();
                String language = LanguagePrefManager.getString(Constants.SELECTED_LANGUAGE,"en");

                for (GetDistanceModel.Data data:getDistanceModel.getData()){

                    if(language.equals("en")){
                        sortList.add(new DistanceListModel
                                (data.getId(),data.getDistance(),data.getMeterKm()));
                    }else {
                        sortList.add(new DistanceListModel
                                (data.getId(),data.getDistance(),data.getMeter_km_ar()));
                    }

                }


                DistanceListAdapter distanceListAdapter = new DistanceListAdapter(sortList,
                        activity,isfromSetting);
                listViewSort.setAdapter(distanceListAdapter);

                // set on item selected
                listViewSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                       DistanceListModel listModel = sortList.get(i);
                       String distance = listModel.getDistance() + " "+
                               listModel.getMeterKm();
                        PrefManager.putString(Constants.PIN_DISTANCE,distance);
                        PrefManager.putString(Constants.PIN_DISTANCE_ID,listModel.getId());
                        if(textView!=null){
                            textView.setText(distance);
                        }
                        PrefManager.saveDistanceList(activity,sortList);
                        saveDistance(distance,sortList);
                        popupWindow.dismiss();
                    }
                });

                imgCancle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popupWindow.dismiss();
                    }
                });


                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });

        ViewGroup root = (ViewGroup)activity.getWindow().getDecorView().getRootView();
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(activity.getDrawable(R.drawable.bg_button_round));
        if(isfromSetting){
            popupWindow.showAsDropDown(viewLayout, 0,-50, Gravity.END);
        }else {

            Display display = activity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            popupWindow.setWidth(width-50);
            popupWindow.showAtLocation(customView, Gravity.BOTTOM, 0, 160);

        }

        applyDim(root,0.6F);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                clearDim(root);
            }
        });

    }

    public static PopupWindow openSaveRouteDialog(Activity activity, List<AddPathWithPin> path, saveRouteClickListener clickListener){
        LayoutInflater layoutInflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popup_save_route,null);

        ImageView imgCancle = (ImageView) customView.findViewById(R.id.img_cancle);
        AutoCompleteTextView category = (AutoCompleteTextView) customView.findViewById(R.id.et_category);
        Button btnSave = (Button) customView.findViewById(R.id.btn_save);
        EditText etRouteName = (EditText) customView.findViewById(R.id.et_route_name);
        Button btnSkip = (Button) customView.findViewById(R.id.btn_skip);
        ImageView imgShare = (ImageView) customView.findViewById(R.id.img_share);
        imgCancle.setVisibility(View.VISIBLE);


        if(path.size()>=2){
            for(int j=0;j<2;j++){
                AddPathWithPin addPathWithPinSource = path.get(0);
                AddPathWithPin addPathWithPinDest = path.get(path.size()-1);

                HashMap<String,String> point1 = addPathWithPinSource.getPathLatLng();
                HashMap<String,String> point2 = addPathWithPinDest.getPathLatLng();

                double lat1 = Double.parseDouble(point1.get("lat"));
                double lng1 = Double.parseDouble(point1.get("lng"));
                startingPointLatLng = new LatLng(lat1,lng1);

                double lat2 = Double.parseDouble(point2.get("lat"));
                double lng2 = Double.parseDouble(point2.get("lng"));

                addSource  = getAddress(activity,lat1,lng1);
                if(!addSource.isEmpty()){
                    addSource = activity.getString(R.string.from) + " " + addSource;
                }
                addDestination  = getAddress(activity,lat2,lng2);
                if(!addDestination.isEmpty()){
                    addDestination =  activity.getString(R.string.to) + " " + addDestination;
                }

            }
        }else if(path.size()==1){
            AddPathWithPin addPathWithPinSource = path.get(0);
            HashMap<String,String> point1 = addPathWithPinSource.getPathLatLng();

            double lat1 = Double.parseDouble(point1.get("lat"));
            double lng1 = Double.parseDouble(point1.get("lng"));
            addSource  = addDestination = getAddress(activity,lat1,lng1);
            if(!addSource.isEmpty()){
                addSource = activity.getString(R.string.from) + " " + addSource;
            }
            if(!addDestination.isEmpty()){
                addDestination =  activity.getString(R.string.to) + " " + addDestination;
            }
            startingPointLatLng = new LatLng(lat1,lng1);

        }


        PopupWindow popupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        imgCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.negativeClick(popupWindow);
            }
        });

        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setCategoryList(activity,category);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String routeName = etRouteName.getText().toString();
                String categoryName = category.getText().toString();
                if(!routeName.isEmpty() &&
                        !categoryName.isEmpty()){
                    clickListener.positiveClick(categoryName,routeName,getNotificationMessage(activity,addSource,
                                    addDestination,routeName,categoryName),addSource,addDestination);
                    popupWindow.dismiss();
                }else {
                    Toast.makeText(activity, activity.getString(R.string.route_error), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.negativeClick(popupWindow);
            }
        });

        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String routeName = etRouteName.getText().toString();
                String categoryName = category.getText().toString();
                if(!routeName.isEmpty() &&
                        !categoryName.isEmpty()){

                    shareCreateRoute(activity,startingPointLatLng.getLatitude(),
                            startingPointLatLng.getLongitude(),
                            addSource,addDestination,routeName,categoryName);
                }else {
                    Toast.makeText(activity, activity.getString(R.string.route_error), Toast.LENGTH_SHORT).show();
                }

            }
        });

    etRouteName.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            popupWindow.setFocusable(true);
            popupWindow.setOutsideTouchable(true);
            popupWindow.update();
            showKeyBoard(activity);
        }
    });

        KeyboardVisibilityEvent.setEventListener(activity,
                new KeyboardVisibilityEventListener() {

            @Override
            public void onVisibilityChanged(boolean isOpen) {
                if (isOpen) {
                    popupWindow.setFocusable(true);
                    popupWindow.setOutsideTouchable(true);
                    popupWindow.update();
                } else {
                    popupWindow.setFocusable(false);
                    popupWindow.setOutsideTouchable(false);
                    popupWindow.update();
                    UIUtil.hideKeyboard(activity);
                }
            }
        });
        etRouteName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {

            }
        });

        ViewGroup root = (ViewGroup)activity.getWindow().getDecorView().getRootView();
        popupWindow.setBackgroundDrawable(activity.getDrawable(R.drawable.bg_button_round));
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        popupWindow.setWidth(width-50);
        popupWindow.setFocusable(false);
        popupWindow.setOutsideTouchable(false);
        popupWindow.showAtLocation(customView, Gravity.BOTTOM, 0, 160);
        applyDim(root,0.6F);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                clearDim(root);
            }
        });
        return popupWindow;

    }

    private static String getNotificationMessage(Activity activity,String sourceName,String destiName,
                                     String routeName,String routeType){
        return activity.getString(R.string.route_name_) + " " + routeName + "\n" +
                activity.getString(R.string.route_type) + " " + routeType + "\n\n" +
                activity.getString(R.string.create_new_route) + " " +sourceName + " " +destiName + "\n";
    }

    public static String getAddress(Activity activity, double lat, double lng) {
        String language = LanguagePrefManager.getString(Constants.SELECTED_LANGUAGE,"en");
        Geocoder geocoder = new Geocoder(activity, new Locale(language));
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String   add = obj.getAddressLine(0);

            Log.d("IGA", "Address" + add);
            return add;

            // TennisAppActivity.showDialog(add);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }


    public static void saveRouteList(Context context, List<GetMyRoutesModel.Data> routeModelList) {
        SharedPreferences mPrefs = context.getSharedPreferences(Constants.SAVE_ROUTE_LIST, context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(routeModelList);
        prefsEditor.putString("myJson", json);
        prefsEditor.commit();
    }



    public static List<GetMyRoutesModel.Data> loadSavedRouteList(Context context) {
        List<GetMyRoutesModel.Data> routeModelList = new ArrayList<GetMyRoutesModel.Data>();
        SharedPreferences mPrefs = context.getSharedPreferences(Constants.SAVE_ROUTE_LIST, context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("myJson", "");
        if (json.isEmpty()) {
            routeModelList = new ArrayList<GetMyRoutesModel.Data>();
        } else {
            Type type = new TypeToken<List<GetMyRoutesModel.Data>>() {
            }.getType();
            routeModelList = gson.fromJson(json, type);
        }
        return routeModelList;
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.d(TAG, "ParseException - dateFormat");
        }

        return outputDate;

    }

    public static void saveHistoryList(Context context, List<GetMyRoutesModel.Data> routeModelList) {
        SharedPreferences mPrefs = context.getSharedPreferences(Constants.HISTORY_ROUTE_LIST, context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(routeModelList);
        prefsEditor.putString("history", json);
        prefsEditor.commit();
    }

    public static List<GetMyRoutesModel.Data> loadHistoryRouteList(Context context) {
        List<GetMyRoutesModel.Data> routeModelList = new ArrayList<GetMyRoutesModel.Data>();
        SharedPreferences mPrefs = context.getSharedPreferences(Constants.HISTORY_ROUTE_LIST, context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("history", "");
        if (json.isEmpty()) {
            routeModelList = new ArrayList<GetMyRoutesModel.Data>();
        } else {
            Type type = new TypeToken<List<GetMyRoutesModel.Data>>() {
            }.getType();
            routeModelList = gson.fromJson(json, type);
        }
        return routeModelList;
    }

    public static void checkLockedUserApi(Activity activity){
        APIManager.checkUserLockedAPi(new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                LockedUserModel lockedUserModel = (LockedUserModel) modelclass;
                if(lockedUserModel.getUser()!=null){
                    String userActiveStatus = lockedUserModel.getUser().get(0).getStatus();
                    if(userActiveStatus.equals("0")){
                        showLockedAlertDialog(activity, activity.getString(R.string.locked_user),
                                activity.getString(R.string.user_locked), new dialogPositiveClickListener() {
                                    @Override
                                    public void positiveClick() {
                                        PrefManager.deleteuser();
                                        activity.startActivity(new Intent(activity, LoginScreenActivity.class).
                                                setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                        activity.finishAffinity();
                                    }

                                    @Override
                                    public void negativeClick() {

                                    }
                                });
                    }
                }
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }


    public static ArrayList<GetMyRoutesModel.Data> removeDuplicates(List<GetMyRoutesModel.Data> list){
        Set<GetMyRoutesModel.Data> set = new TreeSet(new Comparator<GetMyRoutesModel.Data>() {

            @Override
            public int compare(GetMyRoutesModel.Data o1, GetMyRoutesModel.Data o2) {
                if(o1.getName().equalsIgnoreCase(o2.getName())){
                    return 0;
                }
                return 1;
            }
        });
        set.addAll(list);

        final ArrayList newList = new ArrayList(set);
        return newList;
    }

    public static void  setCategoryList(Activity activity,AutoCompleteTextView autoCompleteTextView){
        ArrayList<String> categoryList = new ArrayList<String>();
        categoryList.add(activity.getString(R.string.short_route));
        categoryList.add(activity.getString(R.string.long_route));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                R.layout.list_textview, categoryList);
        autoCompleteTextView.setAdapter(adapter);
        autoCompleteTextView.showDropDown();
    }

    public static void saveDistance(String distance, ArrayList<DistanceListModel> sortList){
        int dist = 0;
        String type = "";
        for(DistanceListModel listModel:sortList){
            String item = listModel.getDistance() + " "+
                    listModel.getMeterKm();
            if(item.equals(distance)){
                dist = Integer.parseInt(listModel.getDistance());
                type = listModel.getMeterKm();
            }
        }
        PrefManager.putInt(Constants.SELECTED_DISTANCE,dist);
        PrefManager.putString(Constants.DISTNACE_TYPE,type);
        Constants.IS_DISTANCE_CHANGE = true;

       /* if(distance.equals(activity.getString(R.string._5km))){
            dist = 5;
        }else if(distance.equals(activity.getString(R.string._10km))){
            dist = 10;
        }else if(distance.equals(activity.getString(R.string._15km))){
            dist = 15;
        }else if(distance.equals(activity.getString(R.string._20km))){
            dist = 20;
        }else if(distance.equals(activity.getString(R.string._30km))){
            dist = 30;
        }else if(distance.equals(activity.getString(R.string._40km))){
            dist = 40;
        }else if(distance.equals(activity.getString(R.string._50km))){
            dist = 50;
        }
        PrefManager.putInt(Constants.SELECTED_DISTANCE,dist);
        Constants.IS_DISTANCE_CHANGE = true;*/
    }

    public static void applyDim(@NonNull ViewGroup parent, float dimAmount){
        Drawable dim = new ColorDrawable(Color.BLACK);
        dim.setBounds(0, 0, parent.getWidth(), parent.getHeight());
        dim.setAlpha((int) (255 * dimAmount));

        ViewGroupOverlay overlay = parent.getOverlay();
        overlay.add(dim);
    }

    public static void clearDim(@NonNull ViewGroup parent) {
        ViewGroupOverlay overlay = parent.getOverlay();
        overlay.clear();
    }


    public static void setLocale (Activity context,String language){
        LanguagePrefManager.putString(Constants.SELECTED_LANGUAGE,language);
        Locale locale;
        if(language.equals("not-set")){ //use any value for default
            locale = Locale.getDefault();
        }
        else {
            locale = new Locale(language);
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLayoutDirection(locale);
        }
        context.getBaseContext().getResources().updateConfiguration(config,
                context.getBaseContext().getResources().getDisplayMetrics());
    }

    public static void shareCreateRoute(Activity activity,double latitude,double longitude,
                                        String sourceName,String destinationName,String routeName,
                                        String routeType){

        String uri = "https://www.google.com/maps/?q=" + latitude+ "," +longitude ;
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,  uri + "\n\n" + getMessage(activity,
                sourceName,destinationName,routeName,routeType));
        activity.startActivity(Intent.createChooser(sharingIntent, "Share in..."));
    }

    private static String getMessage(Activity activity,String sourceName,String destiName,
                                     String routeName,String routeType){
        return activity.getString(R.string.route_name_) + " " + routeName + "\n" +
                activity.getString(R.string.route_type) + " " + routeType + "\n\n" +
                activity.getString(R.string.create_new_route) + " " +sourceName + " " +destiName + "\n\n" +
                activity.getString(R.string.playstorelink);
    }

}
