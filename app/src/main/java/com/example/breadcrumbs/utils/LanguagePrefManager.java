package com.example.breadcrumbs.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.breadcrumbs.app.MyAPP;
import com.example.breadcrumbs.model.SignUpDetails;
import com.google.gson.Gson;

public class LanguagePrefManager {


    public static void deleteuser(){
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs_language", 0);
        sharedPreferences.edit().clear().apply();

    }

    public static void putString(String key, String value) {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs_language", 0);
        checkForNullKey(key);
        checkForNullValue(value);
        sharedPreferences.edit().putString(key, value).apply();
    }

    public static String getString(String key,String defaultvalue) {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs_language", 0);
        return sharedPreferences.getString(key, defaultvalue);
    }

    public static void putBoolean(String key, boolean value) {

        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs_language", 0);
        checkForNullKey(key);
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public static boolean getBoolean(String key)
    {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs_language", 0);
        return sharedPreferences.getBoolean(key, false);

    }



    public static void checkForNullKey(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
    }


    public static void checkForNullValue(String value) {
        if (value == null) {
            throw new NullPointerException();
        }
    }

}
