package com.example.breadcrumbs.utils;

import android.app.Activity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import com.example.breadcrumbs.R;


public class ClickableText {

    public static void setClickableString(Activity activity,String clickableValue, String wholeValue,
                                          TextView yourTextView, textClickListener textClickListener){
        String value = wholeValue;
        SpannableString spannableString = new SpannableString(value);
        int startIndex = value.indexOf(clickableValue);
        int endIndex = startIndex + clickableValue.length();
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(activity.getResources().getColor(R.color.orange_dark));
            }

            @Override
            public void onClick(View widget) {
                // do what you want with clickable value
                textClickListener.textClicked();
            }
        }, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        yourTextView.setText(spannableString);
        yourTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public interface textClickListener{
        void textClicked();
    }
}
