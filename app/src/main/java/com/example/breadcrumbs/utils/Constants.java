package com.example.breadcrumbs.utils;

import com.example.breadcrumbs.model.GetMyRoutesModel;

public class Constants {
    public static final String SIGNUP_DETAILS = "signup_details" ;
    public static final int LOCATIONS = 123;
    public static final int REQUESTCODE_TURNON_GPS = 100;
    public static final String PIN_DISTANCE = "pin_distance";
    public static final String PIN_DISTANCE_ID = "pin_distance_id";
    public static final String NOTIFICATION_STATUS = "notification_status";
    public static final String FIREBASE_TOKEN = "fcm_token";
    public static final String SELECTED_LANGUAGE = "language" ;
    public static final String IS_FROM = "is_from" ;
    public static final String SIGN_UP = "signup";
    public static final String FORGOT_PASSWORD = "forgot_password";
    public static final String START_LOCATION = "start_location" ;
    public static final String START = "start" ;
    public static final String STOP = "stop" ;
    public static final String PIN = "pin";
    public static final String USER_ID = "user_id" ;
    public static final String SELECTED_DISTANCE = "selected_distance";
    public static final String SAVE_ROUTE_LIST = "save_route_list" ;
    public static final String HISTORY_ROUTE_LIST = "history_list" ;
    public static final String DISTANCE_LIST = "distance_list" ;
    public static final String ROUTE_DETAIL = "route_detail" ;
    public static final String TOKEN_ID = "token_id";
    public static final String ROUTE_ID = "route_id" ;
    public static final String DISTNACE_TYPE = "distance_type";
    public static  String NOTIFICATION_TITLE = "title";
    public static  String NOTIFICATION_MESSAGE = "New Route Created";
    public static GetMyRoutesModel.Data NOTIFICATION_DATA = null;
    public static  boolean IS_HISTORY_CLICK = false;
    public static  boolean IS_DISTANCE_CHANGE = false;
    public static  boolean IS_EDIT_CLICK = false ;
    public static  String CHANGE_LOCALE = "en";
    public static  boolean REFRESH_APP = false;
    public static  boolean IS_START_LOCATION_CLICK = false;
    public static  boolean IS_STOP_LOCATION_CLICK = false;
    public static  String ROUTE_START_TIME = "";
    public static  String ROUTE_END_TIME = "";
    public static  boolean IS_PIN_LOCATION_CLICK= false;
    public static String  PRIVACY_POLICY= "http://foxyserver.com/breadcrumbs/privacy-policy.php";
    public static String  TERMS_CONDITION= "http://foxyserver.com/breadcrumbs/terms-and-conditions.php";
}
