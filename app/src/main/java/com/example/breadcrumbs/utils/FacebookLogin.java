package com.example.breadcrumbs.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.activity.HomeScreenActivity;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.model.UserRegistrationApi;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

public class FacebookLogin {

    private static final String TAG = "FacebookLogin";
    private static LoginButton loginButton;
    private static Activity activity;
    private static CallbackManager callbackManager ;

    public FacebookLogin(LoginButton loginButton, Activity activity,CallbackManager callbackManager){
        FacebookLogin.activity = activity;
        FacebookLogin.callbackManager = callbackManager;
        FacebookLogin.loginButton = loginButton;
        facebookSdkInitialization();
    }

    public static void facebookSdkInitialization() {
        //logout from facebook

        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

                if (currentAccessToken != null) {
                    LoginManager.getInstance().logOut();
                }

            }
        };

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getUserProfile(loginResult.getAccessToken());

            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

    }

    public static void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        try {
                            String first_name = "";
                            String last_name = "";
                            String email = "";
                            String id ="";
                            if(object.has("id")){
                                id =  object.getString("id");
                            }
                            if(object.has("first_name")){
                                first_name =  object.getString("first_name");
                            }
                            if(object.has("last_name")){
                                last_name =  object.getString("last_name");
                            }
                            if(object.has("email")){
                                email =  object.getString("email");
                            }
                            String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                            logoutFromFacebook();
                            callSocialLoginApi(first_name,last_name,email,image_url,id);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }

    public static void logoutFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // user already logged out
        }
        GraphRequest delPermRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                if(graphResponse!=null){
                    FacebookRequestError error =graphResponse.getError();
                    if(error!=null){
                        Log.e(TAG, error.toString());
                    }else {
                        AccessToken.setCurrentAccessToken(null);
                        Profile.setCurrentProfile(null);
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        });
        Log.d(TAG,"Executing revoke permissions with graph path" + delPermRequest.getGraphPath());
        delPermRequest.executeAsync();
    }

    public static void callSocialLoginApi(String firstName,String lastName,String email,
                                    String profileUrl,String socialId){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.FIRST_NAME, firstName);
        hashMap.put(APIConstant.Parameter.LAST_NAME, lastName);
        hashMap.put(APIConstant.Parameter.EMAIL, email);
        hashMap.put(APIConstant.Parameter.PHONE, "");
        hashMap.put(APIConstant.Parameter.GENDER, "");
        hashMap.put(APIConstant.Parameter.COUNTRY, "");
        hashMap.put(APIConstant.Parameter.ADDRESS, "");
        hashMap.put(APIConstant.Parameter.PASSWORD, "");
        hashMap.put(APIConstant.Parameter.PROFILE_URL,profileUrl);
        hashMap.put(APIConstant.Parameter.SOCIAL_ID, socialId);
        hashMap.put(APIConstant.Parameter.TYPE, "facebook");
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());
        String message =  activity.getString(R.string.facebook_login);
        UserRegistrationApi.fbUserRegistrationAPI(activity, hashMap, message, new UserRegistrationApi.apiResponseListener() {
            @Override
            public void responseSuccess() {
                gotoHomeActivity();
            }
        });
    }

    public static void gotoHomeActivity(){
        activity.startActivity(new Intent(activity, HomeScreenActivity.class).
                setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        |Intent.FLAG_ACTIVITY_CLEAR_TASK));
        activity.finishAffinity();

    }
}
