package com.example.breadcrumbs.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;

import androidx.annotation.NonNull;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.app.MyAPP;
import com.example.breadcrumbs.model.DistanceListModel;
import com.example.breadcrumbs.model.DistanceListWithId;
import com.example.breadcrumbs.model.GetMyRoutesModel;
import com.example.breadcrumbs.model.SignUpDetails;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PrefManager {

    public static void saveLoginUser(SignUpDetails user) {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(user);
        sharedPreferencesEditor.putString("SaveLoginUserDetails", serializedObject);
        sharedPreferencesEditor.apply();
    }

    public static Boolean isUserExist() {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs", Context.MODE_PRIVATE);
        return sharedPreferences.contains("SaveLoginUserDetails");
    }


    public static SignUpDetails loginUser() {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs", 0);
        if (sharedPreferences.contains("SaveLoginUserDetails")) {
            final Gson gson = new Gson();

            return gson.fromJson(sharedPreferences.getString("SaveLoginUserDetails", ""), SignUpDetails.class);
        }
        return null;
    }


    public static void deleteuser(){
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs", 0);
        sharedPreferences.edit().clear().apply();

    }

    public static void putString(String key, String value) {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs", 0);
        checkForNullKey(key);
        checkForNullValue(value);
        sharedPreferences.edit().putString(key, value).apply();
    }

    public static int getInt(String key,int defaultvalue) {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs", 0);
        return sharedPreferences.getInt(key, defaultvalue);
    }

    public static void putInt(String key, int value) {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs", 0);
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public static String getString(String key,String defaultvalue) {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs", 0);
        return sharedPreferences.getString(key, defaultvalue);
    }

    public static void putBoolean(String key, boolean value) {

        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs", 0);
        checkForNullKey(key);
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public static boolean getBoolean(String key)
    {
        SharedPreferences sharedPreferences = MyAPP.getAppContext().getSharedPreferences("breadcrumbs", 0);
        return sharedPreferences.getBoolean(key, false);

    }

    public static ArrayList<DistanceListWithId> distanceListLocaleWise(Activity activity,String locale){
        ArrayList<DistanceListWithId> sortList = new ArrayList<>();
        sortList.add(new DistanceListWithId(getStringByLocal(activity,R.string._5km,locale),R.string._5km));
        sortList.add(new DistanceListWithId(getStringByLocal(activity,R.string._10km,locale),R.string._10km));
        sortList.add(new DistanceListWithId(getStringByLocal(activity,R.string._15km,locale),R.string._15km));
        sortList.add(new DistanceListWithId(getStringByLocal(activity,R.string._20km,locale),R.string._20km));
        sortList.add(new DistanceListWithId(getStringByLocal(activity,R.string._30km,locale),R.string._30km));
        sortList.add(new DistanceListWithId(getStringByLocal(activity,R.string._40km,locale),R.string._40km));
        sortList.add(new DistanceListWithId(getStringByLocal(activity,R.string._50km,locale),R.string._50km));

        return sortList;
    }

    public static void saveDistanceList(Context context, ArrayList<DistanceListModel> sortList) {
        SharedPreferences mPrefs = context.getSharedPreferences(Constants.DISTANCE_LIST, context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(sortList);
        prefsEditor.putString("history", json);
        prefsEditor.commit();
    }

    public static ArrayList<DistanceListModel>  loadDistanceList(Context context) {
        ArrayList<DistanceListModel>  distanceList = new ArrayList<DistanceListModel>();
        SharedPreferences mPrefs = context.getSharedPreferences(Constants.DISTANCE_LIST, context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("history", "");
        if (json.isEmpty()) {
            distanceList = new ArrayList<DistanceListModel>();
        } else {
            Type type = new TypeToken<List<GetMyRoutesModel.Data>>() {
            }.getType();
            distanceList = gson.fromJson(json, type);
        }
        return distanceList;
    }

    public static String checkForGenderLanguage(Activity activity, String gender, String language){
        String femaleEn = PrefManager.getStringByLocal(activity,R.string.female,"en");
        String femaleAr = PrefManager.getStringByLocal(activity,R.string.female,"ar");
        String maleEn = PrefManager.getStringByLocal(activity,R.string.male,"en");
        String maleAr = PrefManager.getStringByLocal(activity,R.string.male,"ar");
        if(language.equals("en")){
            if(gender.equals(femaleAr)){
                return femaleEn;
            }else if(gender.equals(maleAr)){
                return maleEn;
            }else if(gender.equals(femaleEn)){
                return femaleEn;
            }else if(gender.equals(maleEn)){
                return maleEn;
            }
        }else {
            if(gender.equals(femaleAr)){
                return femaleAr;
            }else if(gender.equals(maleAr)){
                return maleAr;
            }else if(gender.equals(femaleEn)){
                return femaleAr;
            }else if(gender.equals(maleEn)){
                return maleAr;
            }
        }
        return "";
    }

    public static String setPrefValueLocaleWise(Activity activity, String locale, String value){
        ArrayList<String> sortList = new ArrayList<>();
        sortList.add(getStringByLocal(activity,R.string._5km,locale));
        sortList.add(getStringByLocal(activity,R.string._10km,locale));
        sortList.add(getStringByLocal(activity,R.string._15km,locale));
        sortList.add(getStringByLocal(activity,R.string._20km,locale));
        sortList.add(getStringByLocal(activity,R.string._30km,locale));
        sortList.add(getStringByLocal(activity,R.string._40km,locale));
        sortList.add(getStringByLocal(activity,R.string._50km,locale));
        for(String dst:sortList){
            if(dst.equals(value)){
                return dst;
            }
        }
        return "";
    }

    public static int getResourceId(Activity activity,String pVariableName, String pResourcename, String pPackageName)
    {
        try {
            return activity.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @NonNull
    public static String getStringByLocal(Activity context, int resId, String locale) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            return getStringByLocalPlus17(context, resId, locale);
        else
            return getStringByLocalBefore17(context, resId, locale);
    }

    @NonNull
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private static String getStringByLocalPlus17(Activity context, int resId, String locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(new Locale(locale));
        return context.createConfigurationContext(configuration).getResources().getString(resId);
    }

    private static String getStringByLocalBefore17(Context context,int resId, String language) {
        Resources currentResources = context.getResources();
        AssetManager assets = currentResources.getAssets();
        DisplayMetrics metrics = currentResources.getDisplayMetrics();
        Configuration config = new Configuration(currentResources.getConfiguration());
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        config.locale = locale;
        /*
         * Note: This (temporarily) changes the devices locale! TODO find a
         * better way to get the string in the specific locale
         */
        Resources defaultLocaleResources = new Resources(assets, metrics, config);
        String string = defaultLocaleResources.getString(resId);
        // Restore device-specific locale
        new Resources(assets, metrics, currentResources.getConfiguration());
        return string;
    }


    public static void checkForNullKey(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
    }


    public static void checkForNullValue(String value) {
        if (value == null) {
            throw new NullPointerException();
        }
    }

}
