package com.example.breadcrumbs.apiUtils;

public class APIConstant {

    public static final String errorNullResponse = "Something went wrong, Please try again!";

    public static final String BASE_URL = "http://foxyserver.com/breadcrumbs/api/";
    public static final String IMAGE_BASE_URL = "http://waitr.foxyserver.com/images/";

    public class API_ENDPOINTS {

        public static final String FORGOT_PASSWORD = "forgot-password.php";
        public static final String LOGOUT = "logout.php";
        public static final String NOTIFICATION = "notification.php";
        public static final String DEVICE_TOKEN = "device.php";
        public static final String CHANGE_PASSWORD = "change-password.php";
        public static final String CREATE_NEW_ROUTE = "create-route.php";
        public static final String GET_MY_ROUTES = "get-routs.php";
        public static final String ABOUT_US = "about-us.php";
        public static final String OTP_VERIFY_FORGOT_PASSWORD = "otp-verify.php";
        public static final String RESEND_OTP = "resend-otp.php";
        public static final String FORGOT_RESEND_OTP = "forgot-resend.php";
        public static final String CLOSE_ACCOUNT = "close-account.php";
        public static final String GET_SELECTED_DISTANCE = "get-distance-arabic.php";
        public static final String OTP_VERIFY = "otp.php";
        public static final String LOGIN = "login.php";
        public static final String SIGNUP = "signup.php";
        public static final String GET_USER_INFO = "get-userprofile.php" ;
        public static final String FACEBOOK_SIGNUP = "fbsignup.php" ;
        public static final String EDIT_USER_INFO = "edit-userinfo.php" ;
        public static final String GET_DISTNCE = "get-distance.php";
        public static final String LOCKED_USER = "logout-user.php" ;
    }

    public class Parameter{

        //signup
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String LANGUAGE = "language";
        public static final String PHONE = "phone";
        public static final String ADDRESS = "address";
        public static final String OTP = "otp";
        public static final String FIRST_NAME = "firstName" ;
        public static final String LAST_NAME = "lastName";
        public static final String GENDER ="gender" ;
        public static final String COUNTRY = "country";
        public static final String ID = "id";
        public static final String PROFILE_URL = "profileUrl" ;
        public static final String SOCIAL_ID = "socialId" ;
        public static final String TYPE = "type" ;
        public static final String NAME = "name" ;
        public static final String SOURCE = "source" ;
        public static final String DESTINATION = "destination" ;
        public static final String CATEGORY = "category" ;
        public static final String ROUTE_LIST = "routelist" ;
        public static final String USER_ID = "userId";
        public static final String METER_KM = "meter_km";
        public static final String DS_ID = "dsID" ;
        public static final String TOKEN = "token" ;
        public static final String STATUS = "status" ;
        public static final String TITLE = "title" ;
        public static final String MESSAGE = "message" ;
        public static final String DATA = "data";
        public static final String START_DATE = "startDate";
        public static final String END_DATE = "endDate";
        public static final String DISTANCE = "distance" ;
    }


    public class KeyParameter {
        public static final String IS_SOCIAL_LOGIN = "is_social_login";
        public static final String USER_ID = "id";
    }
}
