package com.example.breadcrumbs.apiUtils;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface APIInterface {
    @GET
    Call<ResponseBody> requestWithGet(@Url String url, @HeaderMap HashMap<String, String> header, @QueryMap HashMap<String, String> param);

    @FormUrlEncoded
    @POST
    Call<ResponseBody> requestWithPost(@Url String url, @HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @Multipart
    @POST
    Call<ResponseBody> requestWithPostImage(@Url String url, @HeaderMap HashMap<String, String> header,
                                            @PartMap HashMap<String, RequestBody> params, @Part MultipartBody.Part image);

}
