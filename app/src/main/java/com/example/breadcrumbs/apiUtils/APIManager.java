package com.example.breadcrumbs.apiUtils;


import android.app.Activity;
import android.content.Context;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.model.AboutUsModel;
import com.example.breadcrumbs.model.CommonResponseModel;
import com.example.breadcrumbs.model.GetDistanceModel;
import com.example.breadcrumbs.model.GetMyRoutesModel;
import com.example.breadcrumbs.model.LockedUserModel;
import com.example.breadcrumbs.model.SendNotification;
import com.example.breadcrumbs.model.SetDeviceToken;
import com.example.breadcrumbs.model.SignupModel;
import com.example.breadcrumbs.model.LoginApiModel;
import com.example.breadcrumbs.model.ProfileInfoModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.PrefManager;
import com.example.breadcrumbs.utils.ViewControll;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class APIManager {

    public static void loginApi(String email,String password,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.EMAIL,email);
        hashMap.put(APIConstant.Parameter.PASSWORD,password);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());

        APICaller.postRequest(APIConstant.API_ENDPOINTS.LOGIN,hashMap, LoginApiModel.class, apiCallBack);
    }

    public static void forgotPasswordApi(String email,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.EMAIL,email);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());

        APICaller.postRequest(APIConstant.API_ENDPOINTS.FORGOT_PASSWORD,hashMap, CommonResponseModel.class, apiCallBack);
    }

    public static void logoutApi(APICaller.APICallBack apiCallBack){
        String userId = PrefManager.getString(APIConstant.KeyParameter.USER_ID,"0");
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.USER_ID,userId);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());

        APICaller.postRequest(APIConstant.API_ENDPOINTS.LOGOUT,hashMap, CommonResponseModel.class, apiCallBack);
    }

    public static void sendNotification(String title,String message,APICaller.APICallBack apiCallBack){
        String routeId = PrefManager.getString(Constants.ROUTE_ID,"0");
        String userId = PrefManager.getString(APIConstant.KeyParameter.USER_ID,"0");
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.TITLE,title);
        hashMap.put(APIConstant.Parameter.MESSAGE, message);
        hashMap.put(APIConstant.Parameter.ID, routeId);
        hashMap.put(APIConstant.Parameter.USER_ID, userId);

        APICaller.postRequest(APIConstant.API_ENDPOINTS.NOTIFICATION,hashMap, SendNotification.class, apiCallBack);
    }

    public static void setDeviceToken(Activity activity, APICaller.APICallBack apiCallBack){
        String userId = PrefManager.getString(APIConstant.KeyParameter.USER_ID,"0");
        String tokenId = PrefManager.getString(Constants.TOKEN_ID,"0");
        String token = PrefManager.getString(Constants.FIREBASE_TOKEN,"");
        String status = PrefManager.getString(Constants.NOTIFICATION_STATUS,activity.getString(R.string._1));
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.TOKEN,token);
        hashMap.put(APIConstant.Parameter.STATUS, status);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());
        hashMap.put(APIConstant.Parameter.USER_ID, userId);
        if(!tokenId.equals("0")){
            hashMap.put(APIConstant.Parameter.ID, tokenId);
        }

        APICaller.postRequest(APIConstant.API_ENDPOINTS.DEVICE_TOKEN,hashMap, SetDeviceToken.class, apiCallBack);
    }


    public static void changePasswordApi(String email,String password,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();

        hashMap.put(APIConstant.Parameter.EMAIL,email);
        hashMap.put(APIConstant.Parameter.PASSWORD,password);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());

        APICaller.postRequest(APIConstant.API_ENDPOINTS.CHANGE_PASSWORD,hashMap, CommonResponseModel.class, apiCallBack);
    }

    public static void saveNewRouteAPI(HashMap<String, String> hashMap,APICaller.APICallBack apiCallBack){

        APICaller.postRequest(APIConstant.API_ENDPOINTS.CREATE_NEW_ROUTE,hashMap, SignupModel.class, apiCallBack);
    }

    public static void getMyRoutesList(String id, APICaller.APICallBack apiCallBack){

        APICaller.getRequest(APIConstant.API_ENDPOINTS.GET_MY_ROUTES+
                        "?language="+ViewControll.getSelectedLanguage()+"&userId="+id,
                new HashMap<String,String>(), GetMyRoutesModel.class, apiCallBack);
    }

    public static void getAboutUs(APICaller.APICallBack apiCallBack){

        APICaller.getRequest(APIConstant.API_ENDPOINTS.ABOUT_US,
                new HashMap<String,String>(), AboutUsModel.class, apiCallBack);
    }

    public static void getDistanceList(APICaller.APICallBack apiCallBack){

        APICaller.getRequest(APIConstant.API_ENDPOINTS.GET_DISTNCE+"?language="+ViewControll.getSelectedLanguage(),
                new HashMap<String,String>(), GetDistanceModel.class, apiCallBack);
    }

    public static void forgotPasswordVerifyOtpApi(String otp,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.OTP,otp);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());

        APICaller.postRequest(APIConstant.API_ENDPOINTS.OTP_VERIFY_FORGOT_PASSWORD,hashMap, CommonResponseModel.class, apiCallBack);
    }

    public static void checkUserLockedAPi(APICaller.APICallBack apiCallBack){
        String userId = PrefManager.getString(APIConstant.KeyParameter.USER_ID,"0");
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.ID,userId);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());

        APICaller.postRequest(APIConstant.API_ENDPOINTS.LOCKED_USER,hashMap, LockedUserModel.class, apiCallBack);
    }

    public static void fbLoginApi(HashMap<String, String> hashMap,APICaller.APICallBack apiCallBack){

        APICaller.postRequest(APIConstant.API_ENDPOINTS.FACEBOOK_SIGNUP,hashMap, SignupModel.class, apiCallBack);
    }

    public static void verifyOtpApi(String otp,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.OTP,otp);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());

        APICaller.postRequest(APIConstant.API_ENDPOINTS.OTP_VERIFY,hashMap, CommonResponseModel.class, apiCallBack);
    }

    public static void resendOtpApi(String email,String id,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.EMAIL,email);
        hashMap.put(APIConstant.Parameter.ID,id);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());

        APICaller.postRequest(APIConstant.API_ENDPOINTS.RESEND_OTP,hashMap, CommonResponseModel.class, apiCallBack);
    }

    public static void forgotResendOtpApi(String email,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.EMAIL,email);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());

        APICaller.postRequest(APIConstant.API_ENDPOINTS.FORGOT_RESEND_OTP,hashMap, CommonResponseModel.class, apiCallBack);
    }


    public static void closeAccountApi(String id,APICaller.APICallBack apiCallBack){

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.ID,id);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());

        APICaller.postRequest(APIConstant.API_ENDPOINTS.CLOSE_ACCOUNT,hashMap, CommonResponseModel.class, apiCallBack);
    }

    public static void getSelectedDistance(String id,APICaller.APICallBack apiCallBack){

        APICaller.postRequest(APIConstant.API_ENDPOINTS.GET_SELECTED_DISTANCE+"?id="+id + "&language="+ViewControll.getSelectedLanguage()
                ,new HashMap<>(), GetDistanceModel.class, apiCallBack);
    }

    public static void signUpApi(HashMap<String, String> hashMap,APICaller.APICallBack apiCallBack){

        APICaller.postRequest(APIConstant.API_ENDPOINTS.SIGNUP,hashMap,SignupModel.class, apiCallBack);
    }

    public static void verifyOtpApi(HashMap<String, RequestBody> hashMap, MultipartBody.Part image,APICaller.APICallBack apiCallBack){

        APICaller.postMultipartRequest(APIConstant.API_ENDPOINTS.OTP_VERIFY,hashMap, image,SignupModel.class, apiCallBack);
    }

    public static void facebookRegApi(HashMap<String, String> hashMap,APICaller.APICallBack apiCallBack){

        APICaller.postRequest(APIConstant.API_ENDPOINTS.FACEBOOK_SIGNUP,hashMap,LockedUserModel.class, apiCallBack);
    }

    public static void getLoginUserInfo(String id,APICaller.APICallBack apiCallBack){

        APICaller.getRequest(APIConstant.API_ENDPOINTS.GET_USER_INFO+"?id="+id + "&language="+ViewControll.getSelectedLanguage(),new HashMap<>(), ProfileInfoModel.class, apiCallBack);
    }

    public static void editProfileApi(HashMap<String, RequestBody> hashMap, MultipartBody.Part image,APICaller.APICallBack apiCallBack){

        APICaller.postMultipartRequest(APIConstant.API_ENDPOINTS.EDIT_USER_INFO,hashMap, image,CommonResponseModel.class, apiCallBack);
    }

}
