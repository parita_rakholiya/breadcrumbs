package com.example.breadcrumbs.interfaces;

import android.widget.PopupWindow;

import java.util.HashMap;
import java.util.List;

public interface saveRouteClickListener {

    void positiveClick(String category, String routeName,String message,String sourceName,String destinationName);
    void negativeClick(PopupWindow popupWindow);

}
