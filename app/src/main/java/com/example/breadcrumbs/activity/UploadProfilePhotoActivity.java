package com.example.breadcrumbs.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.breadcrumbs.R;

public class UploadProfilePhotoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_profile_photo);
    }
}