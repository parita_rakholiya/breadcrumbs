package com.example.breadcrumbs.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.databinding.ActivityOpenMyRoutesBinding;
import com.example.breadcrumbs.map.GetCurrentLocation;
import com.example.breadcrumbs.model.SaveRouteModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.LanguagePrefManager;
import com.example.breadcrumbs.utils.ViewControll;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

public class OpenMyRoutesActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private ActivityOpenMyRoutesBinding binding;
    private Activity activity;
    private String language;
    private SaveRouteModel saveRouteModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        language = LanguagePrefManager.getString(Constants.SELECTED_LANGUAGE,"en");
        ViewControll.setLocale(this,language);
        binding = ActivityOpenMyRoutesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        activity = OpenMyRoutesActivity.this;
        saveRouteModel = (SaveRouteModel) getIntent().getExtras().getSerializable(Constants.ROUTE_DETAIL);
        binding.txtTitle.setText(saveRouteModel.getRouteName());
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        binding.imgBack.setOnClickListener(this);
        new GetCurrentLocation(activity,mMap,saveRouteModel.getPathRouteList());
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
    }
}