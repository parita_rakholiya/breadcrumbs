package com.example.breadcrumbs.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.LocationRequest;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.breadcrumbs.BuildConfig;
import com.example.breadcrumbs.R;
import com.example.breadcrumbs.adapter.MyRoutesDataAdapter;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.databinding.ActivityHomeScreenBinding;
import com.example.breadcrumbs.fragment.HomeFragment;
import com.example.breadcrumbs.fragment.MyProfileFragment;
import com.example.breadcrumbs.fragment.MyRoutesFragment;
import com.example.breadcrumbs.fragment.SettingFragment;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.CommonResponseModel;
import com.example.breadcrumbs.model.SetDeviceToken;
import com.example.breadcrumbs.model.SignupModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.GpsUtils;
import com.example.breadcrumbs.utils.LanguagePrefManager;
import com.example.breadcrumbs.utils.PrefManager;
import com.example.breadcrumbs.utils.ViewControll;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

public class HomeScreenActivity extends AppCompatActivity{

    public ActivityHomeScreenBinding homeScreenBinding;
    private Activity activity;
    private List<Fragment> fragmentList;
    private HomeFragment homeFragment;
    private MyProfileFragment myProfileFragment;
    private MyRoutesFragment myRoutesFragment;
    private SettingFragment settingFragment;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String language = LanguagePrefManager.getString(Constants.SELECTED_LANGUAGE,"en");
        ViewControll.setLocale(this,language);
        homeScreenBinding = DataBindingUtil.setContentView(this,R.layout.activity_home_screen);
        activity = HomeScreenActivity.this;
        initView();
    }

    private void initView() {
        setFragmentList();
        if (ViewControll.isLocationPermissionGranted(activity)) {
            checkGps();
        }
        if(Constants.REFRESH_APP){
            Constants.REFRESH_APP = false;
            homeScreenBinding.navView.setSelectedItemId(R.id.navigation_setting);
            homeScreenBinding.navView.getMenu().
                    findItem(R.id.navigation_setting).setChecked(true);
            homeScreenBinding.navView.getMenu().performIdentifierAction(R.id.navigation_setting, 0);
            ViewControll.loadFragment(activity, settingFragment,getString(R.string.setting),fragmentList);
        }

        setmOnNavigationItemSelectedListener();
        ((HomeScreenActivity)activity).homeScreenBinding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });

        String[] token = {""};
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if(task.isComplete()){
                    token[0] = task.getResult();
                    Log.d("FCM_token==", "onComplete: new Token got: "+token[0] );
                    PrefManager.putString(Constants.FIREBASE_TOKEN,token[0]);
//                    FirebaseMessaging.getInstance().subscribeToTopic(BuildConfig.APPLICATION_ID);
                    setDeviceTokenAPi(activity);
                }
            }
        });

        setNotificationIntent();
    }

    public static void setDeviceTokenAPi(Activity activity){
        Loader.show(activity);
        APIManager.setDeviceToken(activity,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                Loader.hide(activity);
                SetDeviceToken setDeviceToken = (SetDeviceToken) modelclass;
                if(!setDeviceToken.getStatusCode().equalsIgnoreCase("200 OK")){

                    return null;
                }
                PrefManager.putString(Constants.TOKEN_ID,
                        setDeviceToken.getUser().get(0).getId());
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }


    private void setNotificationIntent(){
        Intent intent = getIntent();
        if(intent.hasExtra(Constants.ROUTE_DETAIL)){
            Intent resultIntent = new Intent(activity, NavigationActivity.class);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            resultIntent.putExtra(Constants.ROUTE_DETAIL,
                    intent.getExtras().getSerializable(Constants.ROUTE_DETAIL));
            startActivity(resultIntent);
        }
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.LOCATIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        checkGps();

                    }

                }else {
                    ViewControll.isLocationPermissionGranted(activity);
                    break;
                }
                return;
            }
        }
    }

    private void checkGps(){
        new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {
                // turn on GPS
                if(isGPSEnable){
                    ViewControll.loadFragment(activity, homeFragment,getString(R.string.dash_board),fragmentList);
                }else {
                    Toast.makeText(activity, getString(R.string.turn_on_gps), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.REQUESTCODE_TURNON_GPS) {
                ViewControll.loadFragment(activity, homeFragment,getString(R.string.dash_board),fragmentList);
            }
        }else if (resultCode == Activity.RESULT_CANCELED) {
            if (requestCode == Constants.REQUESTCODE_TURNON_GPS) {
                Toast.makeText(activity, getString(R.string.gps_alert), Toast.LENGTH_SHORT).show();
                checkGps();
            }
        }
    }


    private void setmOnNavigationItemSelectedListener(){
        homeScreenBinding.navView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @SuppressLint("NonConstantResourceId")
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        ViewControll.loadFragment(activity, homeFragment,getString(R.string.dash_board),fragmentList);
                        break;
                    case R.id.navigation_my_routes:
                        ViewControll.loadFragment(activity, myRoutesFragment,getString(R.string.my_routes),fragmentList);
                        break;
                    case R.id.navigation_setting:
                        ViewControll.loadFragment(activity, settingFragment,getString(R.string.setting),fragmentList);
                        break;
                    case R.id.navigation_my_profile:
                        ViewControll.loadFragment(activity, myProfileFragment,getString(R.string.my_profile),fragmentList);
                        break;
                    default:
                        ViewControll.loadFragment(activity, homeFragment,getString(R.string.dash_board),fragmentList);
                }
                return true; // return true;
            }
        });
    }

    private void setFragmentList(){
        homeFragment = HomeFragment.newInstance();
        myRoutesFragment = MyRoutesFragment.newInstance();
        settingFragment = SettingFragment.newInstance();
        myProfileFragment = MyProfileFragment.newInstance();
        fragmentList =  new ArrayList<>();
        fragmentList.add(homeFragment);
        fragmentList.add(myRoutesFragment);
        fragmentList.add(settingFragment);
        fragmentList.add(myProfileFragment);
    }

    @Override
    public void onBackPressed() {
        if(Constants.IS_HISTORY_CLICK){
            myRoutesFragment.backFromHistory();
            Constants.IS_HISTORY_CLICK = false;
            return;
        }

        if(Constants.IS_EDIT_CLICK){
            myProfileFragment.backFromEdit();
            Constants.IS_EDIT_CLICK = false;
            return;
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}