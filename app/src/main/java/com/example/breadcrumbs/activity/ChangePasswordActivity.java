package com.example.breadcrumbs.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.databinding.ActivityChangePasswordBinding;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.CommonResponseModel;
import com.example.breadcrumbs.model.SignUpDetails;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.ViewControll;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import ir.shahabazimi.instagrampicker.PickerViewControll;
import ir.shahabazimi.instagrampicker.interfaces.dialogPositiveClickListener;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityChangePasswordBinding changePasswordBinding;
    private Activity activity;
    private String newPassword,confirmPassword;
    private String email;
    private BottomSheetDialog bottomSheetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changePasswordBinding = DataBindingUtil.setContentView(this,R.layout.activity_change_password);
        activity = ChangePasswordActivity.this;
        initView();
    }

    private void initView() {
        email = getIntent().getExtras().getString(APIConstant.Parameter.EMAIL);
        changePasswordBinding.btnConfirm.setOnClickListener(this);
        changePasswordBinding.imgBack.setOnClickListener(this);
        changePasswordBinding.etNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if(s.length()>0){
                    if(ViewControll.isPasswordValidMethod(s.toString())){
                        changePasswordBinding.txtErrorPassword.setVisibility(View.GONE);
                    }else {
                        changePasswordBinding.txtErrorPassword.setVisibility(View.VISIBLE);
                    }
                }else if(s.length()==0){
                    changePasswordBinding.txtErrorPassword.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private boolean checkValidation(){
        newPassword = changePasswordBinding.etNewPassword.getText().toString();
        confirmPassword = changePasswordBinding.etNewConfirmPassword.getText().toString();
        if(newPassword.isEmpty() || !ViewControll.isPasswordValidMethod(newPassword)){
            Toast.makeText(activity, getString(R.string.strong_password), Toast.LENGTH_SHORT).show();
            changePasswordBinding.txtErrorPassword.setVisibility(View.VISIBLE);
            return false;
        }else if(confirmPassword.isEmpty() || !confirmPassword.equals(newPassword)){
            Toast.makeText(activity, getString(R.string.password_not_match), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void resetPasswordAlert(){
        ViewControll.showAlertDialog(activity, getString(R.string.reset_password),
                getString(R.string.reset_alert), new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                        ForgotPasswordActivity.getInstance().finish();
                        finish();
                    }

                    @Override
                    public void negativeClick() {

                    }
                });
    }

    @Override
    public void onBackPressed() {
        resetPasswordAlert();
    }

    private void callResetPasswordApi(){
        Loader.show(activity);
        APIManager.changePasswordApi(email,newPassword,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                CommonResponseModel commonResponseModel = (CommonResponseModel) modelclass;
                Loader.hide(activity);
                if(!commonResponseModel.getStatusCode().equalsIgnoreCase("200 ok")){
                    Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                ForgotPasswordActivity.getInstance().finish();
                bottomSheetDialog =  openResetSucessBottomDialog();
                bottomSheetDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                });
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.btn_confirm){
            if(checkValidation()){
                callResetPasswordApi();
            }
        }
        if(id==R.id.img_back){
           onBackPressed();
        }
    }

    private BottomSheetDialog openResetSucessBottomDialog(){
        return PickerViewControll.showBottomSheetDialog(activity, getString(R.string.reset_pass_success),
                getString(R.string.login),false, new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                        finish();
                    }

                    @Override
                    public void negativeClick() {

                    }
                });
    }
}