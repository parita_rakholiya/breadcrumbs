package com.example.breadcrumbs.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.databinding.ActivitySplashScreenBinding;
import com.example.breadcrumbs.model.GetMyRoutesModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.LanguagePrefManager;
import com.example.breadcrumbs.utils.PrefManager;
import com.example.breadcrumbs.utils.ViewControll;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;

public class SplashScreenActivity extends AppCompatActivity {

    private ActivitySplashScreenBinding splashScreenBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String language = LanguagePrefManager.getString(Constants.SELECTED_LANGUAGE,"en");
        ViewControll.setLocale(this,language);
        splashScreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);
        activity = SplashScreenActivity.this;
        initView();
    }

    private void initView() {
        Gson gson = new Gson();
        Bundle extras = getIntent().getExtras();
        Type type = new TypeToken<GetMyRoutesModel.Data>() {
        }.getType();
        if (extras != null) {
            String messageData = extras.getString("message");
            if(messageData!=null){
                GetMyRoutesModel.Data data  = gson.fromJson(messageData, type);
                Intent resultIntent = new Intent(activity, HomeScreenActivity.class);
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                resultIntent.putExtra(Constants.ROUTE_DETAIL,data);
                startActivity(resultIntent);
            }else{
                goToHomeActvity();
            }
        }else{
           goToHomeActvity();
        }

    }

    private void goToHomeActvity(){
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if(PrefManager.isUserExist()){
                    startActivity(new Intent(activity, HomeScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }else {
                    startActivity(new Intent(activity, LoginScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            }
        },3000);
    }
}