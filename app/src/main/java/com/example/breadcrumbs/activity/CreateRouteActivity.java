package com.example.breadcrumbs.activity;

import static com.example.breadcrumbs.utils.PrefManager.getResourceId;
import static com.example.breadcrumbs.utils.PrefManager.getStringByLocal;
import static com.example.breadcrumbs.utils.ViewControll.openDistanceListDailog;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.PopupWindow;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.databinding.ActivityCreateRouteBinding;
import com.example.breadcrumbs.map.GetCurrentLocation;
import com.example.breadcrumbs.model.DistanceListWithId;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.LanguagePrefManager;
import com.example.breadcrumbs.utils.PrefManager;
import com.example.breadcrumbs.utils.ViewControll;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import ir.shahabazimi.instagrampicker.interfaces.dialogPositiveClickListener;

public class CreateRouteActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private ActivityCreateRouteBinding binding;
    private Activity activity;
    private String language;
    private GetCurrentLocation getCurrentLocation;
    public static ProgressDialog progressDialog;
    private PopupWindow startDialog,stopDialog,pinDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        language = LanguagePrefManager.getString(Constants.SELECTED_LANGUAGE,"en");
        ViewControll.setLocale(this,language);
        binding = ActivityCreateRouteBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        activity = CreateRouteActivity.this;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        ViewControll.checkLockedUserApi(activity);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Constants.IS_START_LOCATION_CLICK =false;
        Constants.IS_STOP_LOCATION_CLICK =false;
        binding.imgBack.setOnClickListener(this);
        binding.llDistance.setOnClickListener(this);
        binding.llStart.setOnClickListener(this);
        binding.llEnd.setOnClickListener(this);
        binding.llPin.setOnClickListener(this);
        String prevSelecteddistance = PrefManager.getString(Constants.PIN_DISTANCE,"0");
        if(!prevSelecteddistance.equals("0")){
            binding.txtDistance.setText(prevSelecteddistance);
        }
        getCurrentLocation = new GetCurrentLocation(activity,mMap);
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
        if(id==R.id.ll_start){
            if(!Constants.IS_STOP_LOCATION_CLICK && !Constants.IS_START_LOCATION_CLICK){
                if(startDialog==null || !startDialog.isShowing()){
                    checkIfAnYDialogOpen();
                    startClick();
                }
            }
        }
        if(id==R.id.ll_end){
            if(!Constants.IS_STOP_LOCATION_CLICK && Constants.IS_START_LOCATION_CLICK){
                if(stopDialog==null || !stopDialog.isShowing()) {
                    checkIfAnYDialogOpen();
                    stopClick();
                }
            }
        }

        if(id==R.id.ll_pin){
            if(!Constants.IS_STOP_LOCATION_CLICK && Constants.IS_START_LOCATION_CLICK){
                if(pinDialog==null || !pinDialog.isShowing()) {
                    checkIfAnYDialogOpen();
                    pinClick();
                }
            }
        }

        if(id==R.id.ll_distance){
            checkIfAnYDialogOpen();
            openDistanceListDailog(activity,false,
                    binding.txtDistance,binding.llMain,binding.llMain);
        }
    }

    private void checkIfAnYDialogOpen(){
        if(pinDialog!=null && pinDialog.isShowing()){
            pinDialog.dismiss();
        }else if(startDialog!=null && startDialog.isShowing()){
            startDialog.dismiss();
        }else if(stopDialog!=null && stopDialog.isShowing()){
            stopDialog.dismiss();
        }
    }

    private void stopClick(){
       stopDialog = ViewControll.openStartStopRouteDialog(activity,
                activity.getResources().getString(R.string.stop_route_alert), Constants.STOP, new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                        Constants.IS_STOP_LOCATION_CLICK = true;
                        Constants.IS_START_LOCATION_CLICK = false;
                        binding.llStart.setEnabled(true);
                        Constants.ROUTE_END_TIME = getCurrentTime();
                        showProgressDialog();
                    }

                    @Override
                    public void negativeClick() {

                    }
                });
    }

    private void showProgressDialog(){
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void startClick(){
       startDialog = ViewControll.openStartStopRouteDialog(activity,
                activity.getString(R.string.create_route_alert),Constants.START, new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                        Constants.IS_START_LOCATION_CLICK = true;
                        binding.llStart.setEnabled(false);
                        Constants.ROUTE_START_TIME = getCurrentTime();
                    }

                    @Override
                    public void negativeClick() {

                    }
                });
    }

    private String getCurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa");
        String datetime = dateformat.format(c.getTime());
        return datetime;
    }

    private void pinClick(){
       pinDialog =  ViewControll.openStartStopRouteDialog(activity,
                activity.getResources().getString(R.string.pin_current_location), Constants.PIN, new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                        getCurrentLocation.addPinToCurrentLocation(mMap,GetCurrentLocation.currentLocationLatlng);
                    }

                    @Override
                    public void negativeClick() {

                    }
                });
    }

    @Override
    public void onBackPressed() {
        if(Constants.IS_START_LOCATION_CLICK){
            checkIfAnYDialogOpen();
            binding.llEnd.performClick();
        }else if(GetCurrentLocation.saveRoutePopup!=null && GetCurrentLocation.saveRoutePopup.isShowing()){
            ViewControll.showAlertDialog(activity, getString(R.string.save_route),
                    getString(R.string.not_save_route), new dialogPositiveClickListener() {
                        @Override
                        public void positiveClick() {
                            finish();
                        }

                        @Override
                        public void negativeClick() {

                        }
                    });
        }else {
            getCurrentLocation.clearRoute();
            super.onBackPressed();
        }
    }
}