package com.example.breadcrumbs.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.databinding.ActivityVerifyEmailOtpBinding;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.CommonResponseModel;
import com.example.breadcrumbs.model.SignUpDetails;
import com.example.breadcrumbs.model.UserRegistrationApi;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.ViewControll;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.util.HashMap;

import ir.shahabazimi.instagrampicker.PickerViewControll;
import ir.shahabazimi.instagrampicker.interfaces.dialogPositiveClickListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class VerifyEmailOtpActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityVerifyEmailOtpBinding verifyEmailOtpBinding;
    private Activity activity;
    private CountDownTimer countDownTimer;
    private SignUpDetails signUpDetails;
    private String otpText;
    MultipartBody.Part profileImage;
    private String isFrom;
    private BottomSheetDialog bottomSheetDialog;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        verifyEmailOtpBinding = DataBindingUtil.setContentView(this,
                R.layout.activity_verify_email_otp);
        activity = VerifyEmailOtpActivity.this;
        initView();
    }

    private void initView() {
        signUpDetails = (SignUpDetails) getIntent().getExtras().getSerializable(Constants.SIGNUP_DETAILS);
        isFrom = getIntent().getExtras().getString(Constants.IS_FROM);
        userId = getIntent().getExtras().getString(Constants.USER_ID);
        if(isFrom.equals(Constants.SIGN_UP)){
            verifyEmailOtpBinding.txtTitle.setText(getString(R.string.sign_up));
        }else {
            verifyEmailOtpBinding.txtTitle.setText(getString(R.string.forgot_password));
        }
        verifyEmailOtpBinding.imgBack.setOnClickListener(this);
        verifyEmailOtpBinding.btnVerify.setOnClickListener(this);
        verifyEmailOtpBinding.txtResend.setOnClickListener(this);
        reverseTimer(30,verifyEmailOtpBinding.txtTimer);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
        if(id==R.id.txt_resend){
            if(isFrom.equals(Constants.SIGN_UP)){
                callResendOtpApi();
            }else {
                callForgotResendOtpApi();
            }
        }
        if(id==R.id.btn_verify){
            if(checkForOtpValidation()){
                if(isFrom.equals(Constants.SIGN_UP)){
                    callVerifyOtpApi();
                }else {
                    callForgotPasswordVerifyOtpApi();
                }
            }else {
                Toast.makeText(activity, getString(R.string.valid_otp), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private BottomSheetDialog openOtpVerifiedDailog(String message,String btnText,boolean isFromForgotPassword){
        return PickerViewControll.showBottomSheetDialog(activity, message,btnText,false, new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                        if(isFromForgotPassword){
                            startActivity(new Intent(activity, ChangePasswordActivity.class).
                                    setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .putExtra(APIConstant.Parameter.EMAIL,signUpDetails.getEmail()));
                            finish();
                        }else{
                            startActivity(new Intent(activity, HomeScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            finishAffinity();
                        }
                    }

                    @Override
                    public void negativeClick() {

                    }
                });
    }

    private boolean checkForOtpValidation(){
        otpText = verifyEmailOtpBinding.otpView.getOTP();
        if(otpText==null || otpText.isEmpty()){
            return false;
        }else if(otpText.length()==4){
            return true;
        }
        return false;
    }

    public void reverseTimer(int Seconds,final TextView tv){
        countDownTimer = new CountDownTimer(Seconds* 1000+1000, 1000) {

            public void onTick(long millisUntilFinished) {
                verifyEmailOtpBinding.llTimer.setVisibility(View.VISIBLE);
                verifyEmailOtpBinding.txtResend.setEnabled(false);
                int seconds = (int) (millisUntilFinished / 1000);
                seconds = seconds % 60;
                tv.setText(String.format("%02d", seconds) +"s");
            }

            public void onFinish() {
                verifyEmailOtpBinding.llTimer.setVisibility(View.GONE);
                verifyEmailOtpBinding.txtResend.setEnabled(true);
            }

        }.start();
    }

    private void callVerifyOtpApi(){
        HashMap<String, RequestBody> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.FIRST_NAME, ViewControll.convertStringToRequestBody(signUpDetails.getFirstName()));
        hashMap.put(APIConstant.Parameter.OTP, ViewControll.convertStringToRequestBody(otpText));
        hashMap.put(APIConstant.Parameter.LAST_NAME, ViewControll.convertStringToRequestBody(signUpDetails.getLastName()));
        hashMap.put(APIConstant.Parameter.EMAIL, ViewControll.convertStringToRequestBody(signUpDetails.getEmail()));
        hashMap.put(APIConstant.Parameter.PHONE, ViewControll.convertStringToRequestBody(signUpDetails.getPhoneNumber()));
        hashMap.put(APIConstant.Parameter.GENDER, ViewControll.convertStringToRequestBody(signUpDetails.getGender()));
        hashMap.put(APIConstant.Parameter.COUNTRY, ViewControll.convertStringToRequestBody(signUpDetails.getCountry()));
        hashMap.put(APIConstant.Parameter.ADDRESS, ViewControll.convertStringToRequestBody(signUpDetails.getAddress()));
        hashMap.put(APIConstant.Parameter.PASSWORD, ViewControll.convertStringToRequestBody(signUpDetails.getPassword()));
        hashMap.put(APIConstant.Parameter.PROFILE_URL,ViewControll.convertStringToRequestBody(""));
        hashMap.put(APIConstant.Parameter.SOCIAL_ID, ViewControll.convertStringToRequestBody(""));
        hashMap.put(APIConstant.Parameter.TYPE, ViewControll.convertStringToRequestBody("normal"));
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.convertStringToRequestBody(ViewControll.getSelectedLanguage()));
        if(signUpDetails.getProfileImage()!=null){
            // Parsing any Media type file
            RequestBody requestBody = RequestBody.create(new File(signUpDetails.getProfileImage()), MediaType.parse("image/*"));
            profileImage = MultipartBody.Part.createFormData("image",
                    new File(signUpDetails.getProfileImage()).getName(), requestBody);

        }

        UserRegistrationApi.userRegistrationAPI(activity, hashMap,"", profileImage, new UserRegistrationApi.apiResponseListener() {
            @Override
            public void responseSuccess() {
               bottomSheetDialog =  openOtpVerifiedDailog(getString(R.string.email_verified),
                       getString(R.string.go_to_dashboard),false);
                bottomSheetDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                });
            }
        });

    }

    private void callForgotPasswordVerifyOtpApi(){
        Loader.show(activity);
        APIManager.forgotPasswordVerifyOtpApi(otpText,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                CommonResponseModel commonResponseModel = (CommonResponseModel) modelclass;
                Loader.hide(activity);
                if(!commonResponseModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                bottomSheetDialog =  openOtpVerifiedDailog(getString(R.string.otp_verified),
                        getString(R.string.reset_password),true);
                bottomSheetDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                });
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }


    private void callResendOtpApi(){
        Loader.show(activity);
        APIManager.resendOtpApi(signUpDetails.getEmail(),userId,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                CommonResponseModel commonResponseModel = (CommonResponseModel) modelclass;
                Loader.hide(activity);
                if(!commonResponseModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }

                Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                reverseTimer(30,verifyEmailOtpBinding.txtTimer);

                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private void callForgotResendOtpApi(){
        Loader.show(activity);
        APIManager.forgotResendOtpApi(signUpDetails.getEmail(),new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                CommonResponseModel commonResponseModel = (CommonResponseModel) modelclass;
                Loader.hide(activity);
                if(!commonResponseModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }

                Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                reverseTimer(30,verifyEmailOtpBinding.txtTimer);

                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

}