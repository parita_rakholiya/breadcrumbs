package com.example.breadcrumbs.activity;

import static com.example.breadcrumbs.utils.ViewControll.checkPhoneNumberValidation;
import static com.example.breadcrumbs.utils.ViewControll.getAssetJsonData;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.databinding.ActivitySignUpBinding;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.CommonResponseModel;
import com.example.breadcrumbs.model.CountryData;
import com.example.breadcrumbs.model.SignUpDetails;
import com.example.breadcrumbs.model.SignupModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.ViewControll;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ir.shahabazimi.instagrampicker.InstagramPicker;
import okhttp3.MultipartBody;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivitySignUpBinding signUpBinding;
    private Activity activity;
    private String email,firstName,lastName,phoneNo,gender,country,password,address,confirmPassword;
    private String profileImagePath;
    MultipartBody.Part profileImage;
    private List<CountryData> countryDataList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signUpBinding = DataBindingUtil.setContentView(this,R.layout.activity_sign_up);
        activity = SignUpActivity.this;
        initView();
    }

    private void initView() {
        countryDataList = ViewControll.getAllCountryInfo(activity);
        signUpBinding.llUploadProfilePhoto.setOnClickListener(this);
        signUpBinding.llNext.setOnClickListener(this);
        signUpBinding.imgBack.setOnClickListener(this);
        signUpBinding.etCountry.setOnClickListener(this);
        signUpBinding.etGender.setOnClickListener(this);
        signUpBinding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if(s.length()>0){
                    if(ViewControll.isPasswordValidMethod(s.toString())){
                        signUpBinding.txtErrorPassword.setVisibility(View.GONE);
                    }else {
                        signUpBinding.txtErrorPassword.setVisibility(View.VISIBLE);
                    }
                }else if(s.length()==0){
                    signUpBinding.txtErrorPassword.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private boolean checkValidation(){
         email = signUpBinding.etEmail.getText().toString().trim();
         password = signUpBinding.etPassword.getText().toString().trim();
         address = signUpBinding.etAddress.getText().toString().trim();
         firstName = signUpBinding.etFirstName.getText().toString().trim();
         lastName = signUpBinding.etLastName.getText().toString().trim();
         phoneNo = signUpBinding.etPhone.getText().toString().trim();
         gender = signUpBinding.etGender.getText().toString().trim();
         country = signUpBinding.etCountry.getText().toString().trim();
         confirmPassword = signUpBinding.etConfirmPassword.getText().toString().trim();
       if(firstName.isEmpty()){
            Toast.makeText(activity, getString(R.string.enter_first_name), Toast.LENGTH_SHORT).show();
            return false;
        }else if(lastName.isEmpty()){
            Toast.makeText(activity, getString(R.string.enter_last_name), Toast.LENGTH_SHORT).show();
            return false;
        }else if(TextUtils.isEmpty(email) || !validateEmailAddress(email)){
            Toast.makeText(activity, getString(R.string.valid_email), Toast.LENGTH_SHORT).show();
            return false;
        }else if(gender.isEmpty()){
            Toast.makeText(activity, getString(R.string.select_gender), Toast.LENGTH_SHORT).show();
            return false;
        }else if(address.isEmpty()){
            Toast.makeText(activity, getString(R.string.enter_address), Toast.LENGTH_SHORT).show();
            return false;
        }else if(country.isEmpty()){
            Toast.makeText(activity, getString(R.string.select_country), Toast.LENGTH_SHORT).show();
            return false;
        }else if(phoneNo.isEmpty() || !checkPhoneNumberValidation(countryDataList,country,phoneNo)){
            Toast.makeText(activity, getString(R.string.phoneno_alert), Toast.LENGTH_SHORT).show();
            return false;
        }else if(password.isEmpty() || !ViewControll.isPasswordValidMethod(password)){
            Toast.makeText(activity, getString(R.string.strong_password), Toast.LENGTH_SHORT).show();
            signUpBinding.txtErrorPassword.setVisibility(View.VISIBLE);
            return false;
        }else if(confirmPassword.isEmpty() || !confirmPassword.equals(password)){
            Toast.makeText(activity, getString(R.string.password_not_match), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateEmailAddress(String emailAddress){
        Matcher matcher =  android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress);
        return matcher.matches();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
        if(id==R.id.et_gender){
            ViewControll.setGenderList(activity,signUpBinding.etGender);
        }
        if(id==R.id.et_country){
            ViewControll.getCountryList(activity,signUpBinding.etCountry);
        }
        if(id==R.id.ll_upload_profile_photo){

            if (ViewControll.isStoragePermissionGranted(activity)) {
                uploadProfilePicture();
            }
        }
        if(id==R.id.ll_next){
            if(checkValidation()){
                saveSignupDetails(email,password,phoneNo,firstName,
                        lastName,gender,address,country,profileImagePath);
            }
        }

    }

    private void saveSignupDetails(String email,String password,String phoneNo,String firstName,
                                   String lastName,String gender,String address,String country,
                                   String profileImage){
        SignUpDetails signUpDetails = new SignUpDetails();
        signUpDetails.setEmail(email);
        signUpDetails.setPassword(password);
        signUpDetails.setPhoneNumber(phoneNo);
        signUpDetails.setFirstName(firstName);
        signUpDetails.setLastName(lastName);
        signUpDetails.setGender(gender);
        signUpDetails.setAddress(address);
        signUpDetails.setCountry(country);
        signUpDetails.setProfileImage(profileImage);
        callSignUpApi(signUpDetails);

    }

    private void uploadProfilePicture(){
        InstagramPicker instagramPicker = new InstagramPicker(activity,false);
        //       this way for just a picture
        instagramPicker.show(1, 1, 1, address -> {
            //        receive image address in here
            Log.d("image==",address.get(0));
            profileImagePath = address.get(0);
            Toast.makeText(activity, getString(R.string.photo_update), Toast.LENGTH_SHORT).show();
        });
    }

    private void callSignUpApi(SignUpDetails signUpDetails){
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.EMAIL,email);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());
        Loader.show(activity);

        APIManager.signUpApi(hashMap,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                SignupModel commonResponseModel = (SignupModel) modelclass;
                Loader.hide(activity);
                if(!commonResponseModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                if(commonResponseModel.getMessage().equalsIgnoreCase(activity.getString(R.string.user_exists))){
                    Loader.hide(activity);
                    Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(activity,VerifyEmailOtpActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(Constants.SIGNUP_DETAILS,signUpDetails)
                .putExtra(Constants.IS_FROM,Constants.SIGN_UP)
                .putExtra(Constants.USER_ID,String.valueOf(commonResponseModel.getUser().get(0).getId())));

                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }



}