package com.example.breadcrumbs.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.breadcrumbs.R
import com.example.breadcrumbs.model.AddPathWithPin
import com.example.breadcrumbs.model.GetMyRoutesModel
import com.example.breadcrumbs.model.SaveRouteModel
import com.example.breadcrumbs.utils.CompanionString.Companion.ICON_ID
import com.example.breadcrumbs.utils.CompanionString.Companion.LAYER_ID
import com.example.breadcrumbs.utils.CompanionString.Companion.SOURCE_ID
import com.example.breadcrumbs.utils.Constants
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.api.directions.v5.DirectionsCriteria.*
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.api.directions.v5.models.RouteOptions
import com.mapbox.api.directions.v5.models.VoiceInstructions
import com.mapbox.api.matching.v5.models.MapMatchingResponse
import com.mapbox.bindgen.Expected
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.mapboxsdk.utils.BitmapUtils
import com.mapbox.maps.*
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.annotation.annotations
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationOptions
import com.mapbox.maps.plugin.annotation.generated.createPointAnnotationManager
import com.mapbox.maps.plugin.gestures.OnMapLongClickListener
import com.mapbox.maps.plugin.gestures.gestures
import com.mapbox.maps.plugin.locationcomponent.location
import com.mapbox.navigation.base.TimeFormat
import com.mapbox.navigation.base.extensions.applyDefaultNavigationOptions
import com.mapbox.navigation.base.extensions.applyLanguageAndVoiceUnitOptions
import com.mapbox.navigation.base.formatter.DistanceFormatterOptions
import com.mapbox.navigation.base.options.NavigationOptions
import com.mapbox.navigation.base.trip.model.RouteProgress
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.directions.session.RoutesObserver
import com.mapbox.navigation.ui.base.util.MapboxNavigationConsumer
import com.mapbox.navigation.ui.maneuver.api.MapboxManeuverApi
import com.mapbox.navigation.ui.maneuver.view.MapboxManeuverView
import com.mapbox.navigation.ui.maps.camera.NavigationCamera
import com.mapbox.navigation.ui.maps.camera.data.MapboxNavigationViewportDataSource
import com.mapbox.navigation.ui.maps.camera.lifecycle.NavigationBasicGesturesHandler
import com.mapbox.navigation.ui.maps.camera.state.NavigationCameraState
import com.mapbox.navigation.ui.maps.camera.state.NavigationCameraStateChangedObserver
import com.mapbox.navigation.ui.maps.camera.view.MapboxRecenterButton
import com.mapbox.navigation.ui.maps.camera.view.MapboxRouteOverviewButton
import com.mapbox.navigation.ui.maps.location.NavigationLocationProvider
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowApi
import com.mapbox.navigation.ui.maps.route.arrow.api.MapboxRouteArrowView
import com.mapbox.navigation.ui.maps.route.arrow.model.RouteArrowOptions
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineApi
import com.mapbox.navigation.ui.maps.route.line.api.MapboxRouteLineView
import com.mapbox.navigation.ui.maps.route.line.model.MapboxRouteLineOptions
import com.mapbox.navigation.ui.maps.route.line.model.RouteLine
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineColorResources
import com.mapbox.navigation.ui.maps.route.line.model.RouteLineResources
import com.mapbox.navigation.ui.tripprogress.api.MapboxTripProgressApi
import com.mapbox.navigation.ui.tripprogress.model.*
import com.mapbox.navigation.ui.tripprogress.view.MapboxTripProgressView
import com.mapbox.navigation.ui.voice.api.MapboxSpeechApi
import com.mapbox.navigation.ui.voice.api.MapboxVoiceInstructionsPlayer
import com.mapbox.navigation.ui.voice.model.SpeechAnnouncement
import com.mapbox.navigation.ui.voice.model.SpeechError
import com.mapbox.navigation.ui.voice.model.SpeechValue
import com.mapbox.navigation.ui.voice.model.SpeechVolume
import com.mapbox.navigation.ui.voice.view.MapboxSoundButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.util.*
import com.mapbox.turf.TurfMeasurement.destination

import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.navigation.core.reroute.RerouteController
import com.mapbox.navigation.core.reroute.RerouteState
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback
import com.mapbox.api.matching.v5.MapboxMapMatching as MapboxMapMatching1
import com.mapbox.api.matching.v5.models.MapMatchingMatching
import com.mapbox.common.module.provider.MapboxInvalidModuleException
import com.mapbox.navigation.base.route.RouterCallback
import com.mapbox.navigation.base.route.RouterFailure
import com.mapbox.navigation.base.route.RouterOrigin
import com.mapbox.navigation.core.MapboxNavigationProvider
import com.mapbox.navigation.core.formatter.MapboxDistanceFormatter
import com.mapbox.navigation.core.trip.session.*
import com.mapbox.navigation.ui.maps.camera.transition.NavigationCameraTransitionOptions


const val EXTRA_NAVIGATION_POINT = "com.mapbox.w3w.sample.ui.map.NavigationActivity.extra.point"

class NavigationActivity : AppCompatActivity() {

    /* ----- Mapbox Maps components ----- */
    private lateinit var mapboxMap: MapboxMap
    private  var mapRouteListCount: Int = 0
    private  var  routeOptions: RouteOptions? = null
    private var routeFirstList : MutableList<DirectionsRoute>  = mutableListOf()

    /* ----- Mapbox Navigation components ----- */
    private lateinit var mapboxNavigation: MapboxNavigation

    // location puck integration
    private val navigationLocationProvider = NavigationLocationProvider()
    var pinLocationList: MutableList<Point> = mutableListOf()

    // camera
    private lateinit var navigationCamera: NavigationCamera
    private lateinit var viewportDataSource: MapboxNavigationViewportDataSource
    private lateinit var permissionsManager: PermissionsManager
    private val pixelDensity = Resources.getSystem().displayMetrics.density
    private val overviewPadding: EdgeInsets by lazy {
        EdgeInsets(
            140.0 * pixelDensity,
            40.0 * pixelDensity,
            120.0 * pixelDensity,
            40.0 * pixelDensity
        )
    }
    private val landscapeOverviewPadding: EdgeInsets by lazy {
        EdgeInsets(
            30.0 * pixelDensity,
            380.0 * pixelDensity,
            20.0 * pixelDensity,
            20.0 * pixelDensity
        )
    }
    private val followingPadding: EdgeInsets by lazy {
        EdgeInsets(
            180.0 * pixelDensity,
            40.0 * pixelDensity,
            150.0 * pixelDensity,
            40.0 * pixelDensity
        )
    }
    private val landscapeFollowingPadding: EdgeInsets by lazy {
        EdgeInsets(
            30.0 * pixelDensity,
            380.0 * pixelDensity,
            110.0 * pixelDensity,
            40.0 * pixelDensity
        )
    }

    // trip progress bottom view
    private lateinit var tripProgressApi: MapboxTripProgressApi

    // voice instructions
    private var isVoiceInstructionsMuted = false
    private lateinit var maneuverApi: MapboxManeuverApi
    private lateinit var speechAPI: MapboxSpeechApi
    private lateinit var voiceInstructionsPlayer: MapboxVoiceInstructionsPlayer

    // route line
    private lateinit var routeLineAPI: MapboxRouteLineApi
    private lateinit var routeLineView: MapboxRouteLineView
    private lateinit var routeArrowView: MapboxRouteArrowView
    private val routeArrowAPI: MapboxRouteArrowApi = MapboxRouteArrowApi()

    /* ----- Voice instruction callbacks ----- */
    private val voiceInstructionsObserver = object : VoiceInstructionsObserver {
        override fun onNewVoiceInstructions(voiceInstructions: VoiceInstructions) {
            speechAPI.generate(
                voiceInstructions,
                speechCallback
            )
        }
    }

    private val voiceInstructionsPlayerCallback =
        object : MapboxNavigationConsumer<SpeechAnnouncement> {
            override fun accept(value: SpeechAnnouncement) {
                // remove already consumed file to free-up space
                speechAPI.clean(value)
            }
        }

    private val speechCallback =
        MapboxNavigationConsumer<Expected<SpeechError, SpeechValue>> { expected ->
            expected.fold(
                { error ->
                    // play the instruction via fallback text-to-speech engine
                    voiceInstructionsPlayer.play(
                        error.fallback,
                        voiceInstructionsPlayerCallback
                    )
                },
                { value ->
                    // play the sound file from the external generator
                    voiceInstructionsPlayer.play(
                        value.announcement,
                        voiceInstructionsPlayerCallback
                    )
                }
            )
        }

    /* ----- Location and route progress callbacks ----- */
    private val locationObserver = object : LocationObserver {
        var firstLocationUpdateReceived = false

        override fun onNewRawLocation(rawLocation: Location) {
            // not handled
        }

        override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
            val enhancedLocation = locationMatcherResult.enhancedLocation
            // update location puck's position on the map
            navigationLocationProvider.changePosition(
                location = enhancedLocation,
                keyPoints = locationMatcherResult.keyPoints,
            )

            // update camera position to account for new location
            viewportDataSource.onLocationChanged(enhancedLocation)
            viewportDataSource.evaluate()

            // if this is the first location update the activity has received,
            // it's best to immediately move the camera to the current user location
            if (!firstLocationUpdateReceived) {
                firstLocationUpdateReceived = true
                navigationCamera.requestNavigationCameraToOverview(
                    stateTransitionOptions = NavigationCameraTransitionOptions.Builder()
                        .maxDuration(0) // instant transition
                        .build()
                )
            }
        }
    }

    private val routeProgressObserver = RouteProgressObserver { routeProgress ->
        // update the camera position to account for the progressed fragment of the route
        viewportDataSource.onRouteProgressChanged(routeProgress)
        viewportDataSource.evaluate()

        // draw the upcoming maneuver arrow on the map
        val style = mapboxMap.getStyle()
        if (style != null) {
            val maneuverArrowResult = routeArrowAPI.addUpcomingManeuverArrow(routeProgress)
            routeArrowView.renderManeuverUpdate(style, maneuverArrowResult)
        }

        // update top banner with maneuver instructions
        val maneuvers = maneuverApi.getManeuvers(routeProgress)
        maneuvers.fold(
            { error ->
                Toast.makeText(
                    this@NavigationActivity,
                    error.errorMessage,
                    Toast.LENGTH_SHORT
                ).show()
            },
            {
                findViewById<MapboxManeuverView>(R.id.maneuverView).visibility = View.VISIBLE
                findViewById<MapboxManeuverView>(R.id.maneuverView).renderManeuvers(maneuvers)
            }
        )

        // update bottom trip progress summary
        findViewById<MapboxTripProgressView>(R.id.tripProgressView).visibility = View.GONE
        findViewById<MapboxTripProgressView>(R.id.tripProgressView).render(
            tripProgressApi.getTripProgress(routeProgress)
        )
    }


    private val routesObserver = RoutesObserver { routes ->

        if (routeFirstList.isNotEmpty()) {
            val routeLineList  = mutableListOf<RouteLine>()
            routeFirstList.forEachIndexed { index, directionsRoute ->
                // generate route geometries asynchronously and render them
                CoroutineScope(Dispatchers.Main).launch {
                    routeLineList.add(RouteLine(directionsRoute, null))
                    if (index == routeFirstList.size - 1) {
                        routeLineAPI.setRoutes(
                            routeLineList
                        ) { value ->
                            mapboxMap.getStyle()?.apply {
                                routeLineView.renderRouteDrawData(this, value)
                            }
                        }
                    }
                    // update the camera position to account for the new route
                    viewportDataSource.onRouteChanged(directionsRoute)
                    viewportDataSource.evaluate()
                }

            }

        } else {
            // remove the route line and route arrow from the map
            val style = mapboxMap.getStyle()
            if (style != null) {
                routeLineAPI.clearRouteLine { value ->
                    routeLineView.renderClearRouteLineValue(
                        style,
                        value
                    )
                }
                routeArrowView.render(style, routeArrowAPI.clearArrows())
            }

            // remove the route reference to change camera position
            viewportDataSource.clearRouteData()
            viewportDataSource.evaluate()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        mapboxMap = findViewById<MapView>(R.id.mapView).getMapboxMap()
        findViewById<ImageView>(R.id.stop).visibility = View.GONE
        checkPermissions {

            findViewById<MapboxManeuverView>(R.id.maneuverView).updatePrimaryManeuverTextAppearance(R.style.ManeuverTextAppearance)
            findViewById<MapboxManeuverView>(R.id.maneuverView).updateSecondaryManeuverTextAppearance(R.style.ManeuverTextAppearance)
            findViewById<MapboxManeuverView>(R.id.maneuverView).updateSubManeuverTextAppearance(R.style.ManeuverTextAppearance)
            findViewById<MapboxManeuverView>(R.id.maneuverView).updateStepDistanceTextAppearance(R.style.ManeuverTextAppearance)
            findViewById<MapboxManeuverView>(R.id.maneuverView).updateUpcomingPrimaryManeuverTextAppearance(R.style.ManeuverTextAppearance)
            findViewById<MapboxManeuverView>(R.id.maneuverView).updateUpcomingSecondaryManeuverTextAppearance(R.style.ManeuverTextAppearance)
            findViewById<MapboxManeuverView>(R.id.maneuverView).updateUpcomingManeuverStepDistanceTextAppearance(R.style.ManeuverTextAppearance)

            // initialize the location puck
            findViewById<MapView>(R.id.mapView).location.apply {
                this.locationPuck = LocationPuck2D(
                    bearingImage = ContextCompat.getDrawable(
                        this@NavigationActivity,
                        R.drawable.mapbox_navigation_puck_icon
                    )
                )
                setLocationProvider(navigationLocationProvider)
                enabled = true
            }

            // initialize Mapbox Navigation
            mapboxNavigation = if (MapboxNavigationProvider.isCreated()) {
                MapboxNavigationProvider.retrieve()
            } else {
                MapboxNavigationProvider.create(
                    NavigationOptions.Builder(this.applicationContext)
                        .accessToken(getString(R.string.mapbox_access_token))
                        .build()
                )
            }

//            // Navigate to provided destination
//            val destination = intent.getSerializableExtra(EXTRA_NAVIGATION_POINT) as? Point
//            destination?.let {
//                findRoute(destination)
//            }

            val saveRouteModel = intent.getSerializableExtra(Constants.ROUTE_DETAIL) as? GetMyRoutesModel.Data
            saveRouteModel?.let {
                findRouteList(saveRouteModel)
            }

            val locationService = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return@checkPermissions
            }
            locationService.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                .also {
                    Log.d("NavigationActivity", "init location: $it")
                }?.let { currentLocation ->
                    val cameraOptions = CameraOptions.Builder()
                        .center(Point.fromLngLat(currentLocation.longitude, currentLocation.latitude))
                        .zoom(13.0)
                        .build()
                    mapboxMap.setCamera(cameraOptions)
                }

            // initialize Navigation Camera
            viewportDataSource = MapboxNavigationViewportDataSource(
                findViewById<MapView>(R.id.mapView).getMapboxMap()
            )
            navigationCamera = NavigationCamera(
                findViewById<MapView>(R.id.mapView).getMapboxMap(),
                findViewById<MapView>(R.id.mapView).camera,
                viewportDataSource
            )
            findViewById<MapView>(R.id.mapView).camera.addCameraAnimationsLifecycleListener(
                NavigationBasicGesturesHandler(navigationCamera)
            )
            navigationCamera.registerNavigationCameraStateChangeObserver(
                object : NavigationCameraStateChangedObserver {
                    override fun onNavigationCameraStateChanged(
                        navigationCameraState: NavigationCameraState
                    ) {
                        // shows/hide the recenter button depending on the camera state
                        when (navigationCameraState) {
                            NavigationCameraState.TRANSITION_TO_FOLLOWING,
                            NavigationCameraState.FOLLOWING -> findViewById<MapboxRecenterButton>(R.id.recenter).visibility =
                                View.INVISIBLE

                            NavigationCameraState.TRANSITION_TO_OVERVIEW,
                            NavigationCameraState.OVERVIEW,
                            NavigationCameraState.IDLE -> findViewById<MapboxRecenterButton>(R.id.recenter).visibility =
                                View.VISIBLE
                        }
                    }
                }
            )
            if (this.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                viewportDataSource.overviewPadding = landscapeOverviewPadding
            } else {
                viewportDataSource.overviewPadding = overviewPadding
            }
            if (this.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                viewportDataSource.followingPadding = landscapeFollowingPadding
            } else {
                viewportDataSource.followingPadding = followingPadding
            }

            // initialize top maneuver view
            maneuverApi = MapboxManeuverApi(
                MapboxDistanceFormatter(DistanceFormatterOptions.Builder(this).build())
            )

            // initialize bottom progress view
            tripProgressApi = MapboxTripProgressApi(
                TripProgressUpdateFormatter.Builder(this)
                    .distanceRemainingFormatter(
                        DistanceRemainingFormatter(
                            mapboxNavigation.navigationOptions.distanceFormatterOptions
                        )
                    )
                    .timeRemainingFormatter(TimeRemainingFormatter(this))
                    .percentRouteTraveledFormatter(PercentDistanceTraveledFormatter())
                    .estimatedTimeToArrivalFormatter(
                        EstimatedTimeToArrivalFormatter(this, TimeFormat.NONE_SPECIFIED)
                    )
                    .build()
            )

            // initialize voice instructions
            speechAPI = MapboxSpeechApi(
                this,
                getString(R.string.mapbox_access_token),
                Locale.US.language
            )
            voiceInstructionsPlayer = MapboxVoiceInstructionsPlayer(
                this,
                getString(R.string.mapbox_access_token),
                Locale.US.language
            )

            val customColorResources = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                RouteLineColorResources.Builder()
                    .routeDefaultColor(getColor(R.color.orange_light))
                    .routeCasingColor(getColor(R.color.orange_dark))
                    .routeUnknownCongestionColor(getColor(R.color.orange_light))
                    .routeLowCongestionColor(getColor(R.color.orange_light))
                    .alternativeRouteDefaultColor(getColor(R.color.orange_light))
                    .alternativeRouteLowCongestionColor(getColor(R.color.orange_light))
                    .alternativeRouteCasingColor(getColor(R.color.orange_dark))
                    .alternativeRouteUnknownCongestionColor(getColor(R.color.orange_light))
                    .build()
            }else {
                RouteLineColorResources.Builder()
                    .routeDefaultColor(resources.getColor(R.color.orange_light))
                    .routeCasingColor(resources.getColor(R.color.orange_dark))
                    .routeUnknownCongestionColor(resources.getColor(R.color.orange_light))
                    .routeLowCongestionColor(resources.getColor(R.color.orange_light))
                    .alternativeRouteDefaultColor(resources.getColor(R.color.orange_light))
                    .alternativeRouteLowCongestionColor(resources.getColor(R.color.orange_light))
                    .alternativeRouteCasingColor(resources.getColor(R.color.orange_dark))
                    .alternativeRouteUnknownCongestionColor(resources.getColor(R.color.orange_light))
                    .build()
            }
            val routeLineResources = RouteLineResources.Builder()
                .routeLineColorResources(customColorResources)
                .build()

            // initialize route line
            val mapboxRouteLineOptions = MapboxRouteLineOptions.Builder(this)
                .withVanishingRouteLineEnabled(false)
                .withRouteLineResources(routeLineResources)
                .withRouteLineBelowLayerId("road-label")
                .build()

            routeLineAPI = MapboxRouteLineApi(mapboxRouteLineOptions)
            routeLineView = MapboxRouteLineView(mapboxRouteLineOptions)
            val routeArrowOptions = RouteArrowOptions.Builder(this).build()
            routeArrowView = MapboxRouteArrowView(routeArrowOptions)

            // load map style
            mapboxMap.loadStyleUri(
                Style.MAPBOX_STREETS,
                object : Style.OnStyleLoaded {
                    override fun onStyleLoaded(style: Style) {
                        // add long click listener that search for a route to the clicked destination
                        findViewById<MapView>(R.id.mapView).gestures.addOnMapLongClickListener(
                            object : OnMapLongClickListener {
                                override fun onMapLongClick(point: Point): Boolean {
                                    findRoute(point)
                                    return true
                                }
                            }
                        )

                        //set pined location pin

                        pinLocationList.forEach {
                            addMarkerToMap(it)
                        }
                    }
                }
            )

            // initialize view interactions
            findViewById<ImageView>(R.id.stop).setOnClickListener {
                clearRouteAndStopNavigation()
            }
            findViewById<MapboxRecenterButton>(R.id.recenter).setOnClickListener {
                navigationCamera.requestNavigationCameraToFollowing()
            }
            findViewById<MapboxRouteOverviewButton>(R.id.routeOverview).setOnClickListener {
                navigationCamera.requestNavigationCameraToOverview()
                findViewById<MapboxRecenterButton>(R.id.recenter).showTextAndExtend(2000L)
            }
            findViewById<MapboxSoundButton>(R.id.soundButton).setOnClickListener {
                // mute/unmute voice instructions
                isVoiceInstructionsMuted = !isVoiceInstructionsMuted
                if (isVoiceInstructionsMuted) {
                    findViewById<MapboxSoundButton>(R.id.soundButton).muteAndExtend(2000L)
                    voiceInstructionsPlayer.volume(SpeechVolume(0f))
                } else {
                    findViewById<MapboxSoundButton>(R.id.soundButton).unmuteAndExtend(2000L)
                    voiceInstructionsPlayer.volume(SpeechVolume(1f))
                }
            }

            // start the trip session to being receiving location updates in free drive
            // and later when a route is set, also receiving route progress updates
            mapboxNavigation.startTripSession()
        }
    }

    private fun checkPermissions(onMapReady: () -> Unit) {
        if (PermissionsManager.areLocationPermissionsGranted(this@NavigationActivity)) {
            onMapReady()
        } else {
            permissionsManager = PermissionsManager(object : PermissionsListener {
                override fun onExplanationNeeded(permissionsToExplain: List<String>) {
                    Toast.makeText(
                        this@NavigationActivity, "You need to accept location permissions.",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onPermissionResult(granted: Boolean) {
                    if (granted) {
                        onMapReady()
                    } else {
                        this@NavigationActivity.finish()
                    }
                }
            })
            permissionsManager.requestLocationPermissions(this@NavigationActivity)
        }
    }

    override fun onStart() {
        super.onStart()
        findViewById<MapView>(R.id.mapView).onStart()
        mapboxNavigation.registerRoutesObserver(routesObserver)
        mapboxNavigation.registerRouteProgressObserver(routeProgressObserver)
        mapboxNavigation.registerLocationObserver(locationObserver)
        mapboxNavigation.registerVoiceInstructionsObserver(voiceInstructionsObserver)
//        mapboxNavigation.registerBannerInstructionsObserver(bannerInstructionsObserver)
    }

    override fun onStop() {
        super.onStop()
        findViewById<MapView>(R.id.mapView).onStop()
        mapboxNavigation.unregisterRoutesObserver(routesObserver)
        mapboxNavigation.unregisterRouteProgressObserver(routeProgressObserver)
        mapboxNavigation.unregisterLocationObserver(locationObserver)
        mapboxNavigation.unregisterVoiceInstructionsObserver(voiceInstructionsObserver)
//        mapboxNavigation.unregisterBannerInstructionsObserver(bannerInstructionsObserver)
    }

    override fun onDestroy() {
        super.onDestroy()
        findViewById<MapView>(R.id.mapView).onDestroy()
        mapboxNavigation.onDestroy()
        speechAPI.cancel()
        voiceInstructionsPlayer.shutdown()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        findViewById<MapView>(R.id.mapView).onLowMemory()
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation(): Location? {
        val locationService = applicationContext.getSystemService(LOCATION_SERVICE) as LocationManager
        val providers: List<String> = locationService.getProviders(true)
        var bestLocation: Location? = null
        for (provider in providers) {
            val l: Location = locationService.getLastKnownLocation(provider) ?: continue
            if (bestLocation == null || l.accuracy < bestLocation.accuracy) {
                // Found best last known location: %s", l);
                bestLocation = l
            }
        }
        return bestLocation
    }

    @SuppressLint("MissingPermission")
    private fun findRoute(destination: Point) {
        val origin = getLastKnownLocation().also {
            Log.d("NavigationActivity", "origin location: $it")
        } ?: return

        mapboxNavigation.requestRoutes(
            RouteOptions.builder()
                .applyDefaultNavigationOptions()
                .applyLanguageAndVoiceUnitOptions(this)
                .coordinatesList(
                    listOf(
                        Point.fromLngLat(origin.longitude, origin.latitude),
                        destination
                    )
                )
                .build(),
            object : RouterCallback {
                override fun onCanceled(routeOptions: RouteOptions, routerOrigin: RouterOrigin) {
                }

                override fun onFailure(reasons: List<RouterFailure>, routeOptions: RouteOptions) {
                }

                override fun onRoutesReady(
                    routes: List<DirectionsRoute>,
                    routerOrigin: RouterOrigin
                ) {

                    setRouteAndStartNavigation(routes.first())

                }
            }
        )
    }

    fun RouteOptions.Builder.applyDefaultNavigationOptionsss(): RouteOptions.Builder = apply {
        baseUrl(com.mapbox.core.constants.Constants.BASE_API_URL)
        user(com.mapbox.core.constants.Constants.MAPBOX_USER)
        profile(DirectionsCriteria.PROFILE_DRIVING)
        geometries(DirectionsCriteria.GEOMETRY_POLYLINE6)
        overview(DirectionsCriteria.OVERVIEW_FULL)
        steps(true)
        continueStraight(true)
        roundaboutExits(true)
        annotationsList(
            listOf(
                DirectionsCriteria.ANNOTATION_CONGESTION,
                DirectionsCriteria.ANNOTATION_MAXSPEED,
                DirectionsCriteria.ANNOTATION_SPEED,
                DirectionsCriteria.ANNOTATION_DURATION,
                DirectionsCriteria.ANNOTATION_DISTANCE,
                DirectionsCriteria.ANNOTATION_CLOSURE
            )
        )
        voiceInstructions(true)
        bannerInstructions(true)
//        requestUuid("")
    }


    @SuppressLint("MissingPermission")
    private fun findRouteList(saveRouteModel: GetMyRoutesModel.Data) {
        routeFirstList.clear()
        val origin = getLastKnownLocation().also {
            Log.d("NavigationActivity", "origin location: $it")
        } ?: return

        val pathList: MutableList<Point> = mutableListOf()
        pathList.add(Point.fromLngLat(origin.longitude, origin.latitude))
        val gson = Gson()
        if(saveRouteModel.routelist!=null){
            if(saveRouteModel.routelist.isEmpty()){
                return
            }
        }
        val routeModel = stringToSaveRouteModel(saveRouteModel.routelist,gson)
        pinLocationList = mutableListOf()
        routeModel.pathRouteList.forEachIndexed { index, addPathWithPin ->
            val point = addPathWithPin.pathLatLng
            val lat = point["lat"]!!.toDouble()
            val lng = point["lng"]!!.toDouble()
            pathList.add(Point.fromLngLat(lng, lat))
            if(addPathWithPin.isPinedLocation){
                pinLocationList.add(Point.fromLngLat(lng, lat))
            }
            if(index==0){
                pinLocationList.add(Point.fromLngLat(lng, lat))
            }else if(index==routeModel.pathRouteList.size-1){
                pinLocationList.add(Point.fromLngLat(lng, lat))
            }
        }

        val smallerLists: List<List<Point>> = pathList.toList().chunked(24)
        smallerLists.forEachIndexed { index, list ->
            if(index>=1){
                val newLists = mutableListOf<Point>()
                val prevListPoints =  smallerLists.get(index-1)
                val prevLastPoint = prevListPoints.last()
                newLists.add(prevLastPoint)
                newLists.addAll(list)
                if(index==smallerLists.size-1){
                    setRouteDirection(newLists,true)
                }else{
                    setRouteDirection(newLists,false)
                }
            }else{
                if(smallerLists.size==1){
                    setRouteDirection(smallerLists.get(index),true)
                }else{
                    setRouteDirection(smallerLists.get(index),false)
                }
            }
        }
    }

    private fun setRouteDirection(pointList: List<Point>,isLastList:Boolean){
        mapboxNavigation.requestRoutes(
                 RouteOptions.builder()
                     .applyDefaultNavigationOptions()
                     .applyLanguageAndVoiceUnitOptions(this)
                     .coordinatesList(
                         pointList
                     )
                     .annotationsList(
                         listOf(
                             DirectionsCriteria.ANNOTATION_CONGESTION,
                             DirectionsCriteria.ANNOTATION_MAXSPEED,
                             DirectionsCriteria.ANNOTATION_SPEED,
                             DirectionsCriteria.ANNOTATION_DURATION,
                             DirectionsCriteria.ANNOTATION_DISTANCE
                         )
                     )
                     .build(),
                 object : RouterCallback {
                     override fun onCanceled(
                         routeOptions: RouteOptions,
                         routerOrigin: RouterOrigin
                     ) {
                     }

                     override fun onFailure(
                         reasons: List<RouterFailure>,
                         routeOptions: RouteOptions
                     ) {
                     }

                     override fun onRoutesReady(
                         routes: List<DirectionsRoute>,
                         routerOrigin: RouterOrigin
                     ) {
                         routeFirstList.add(routes.first())

                         mapboxNavigation.resetTripSession()
                         mapboxNavigation.setRoutes(routeFirstList)

                         if(isLastList){
                             setRouteAndStartNavigation(routeFirstList.first())
                         }
                     }
                 }
             )
    }


    private fun stringToSaveRouteModel(data: String,gson:Gson): SaveRouteModel {
        val listType = object : TypeToken<SaveRouteModel>() {
        }.type
        return gson.fromJson(data, listType)
    }


    private fun setRouteAndStartNavigation(route: DirectionsRoute) {

        // show UI elements
        findViewById<MapboxSoundButton>(R.id.soundButton).visibility = View.VISIBLE
        findViewById<MapboxRouteOverviewButton>(R.id.routeOverview).visibility = View.VISIBLE
        findViewById<CardView>(R.id.tripProgressCard).visibility = View.GONE
        findViewById<MapboxRouteOverviewButton>(R.id.routeOverview).showTextAndExtend(2000L)
        findViewById<MapboxSoundButton>(R.id.soundButton).unmuteAndExtend(2000L)

        // move the camera to overview when new route is available

        navigationCamera.requestNavigationCameraToOverview()


    }

    private fun clearRouteAndStopNavigation() {
        // clear
        mapboxNavigation.setRoutes(listOf())

        // hide UI elements
        findViewById<MapboxSoundButton>(R.id.soundButton).visibility = View.INVISIBLE
        findViewById<MapboxManeuverView>(R.id.maneuverView).visibility = View.INVISIBLE
        findViewById<MapboxRouteOverviewButton>(R.id.routeOverview).visibility = View.INVISIBLE
        findViewById<CardView>(R.id.tripProgressCard).visibility = View.INVISIBLE
    }

    private fun addMarkerToMap(point: Point) {
        bitmapFromDrawableRes(
            this@NavigationActivity,
            R.drawable.ic_pin_white
        )?.let {
            Log.d("marker==","added")
            val mapView = findViewById<MapView>(R.id.mapView)
            val annotationApi = mapView?.annotations
            val pointAnnotationManager = annotationApi?.createPointAnnotationManager(mapView)
            val pointAnnotationOptions: PointAnnotationOptions = PointAnnotationOptions()
                .withPoint(point)
                .withIconImage(it)
            pointAnnotationManager?.create(pointAnnotationOptions)
        }
    }
    private fun bitmapFromDrawableRes(context: Context, @DrawableRes resourceId: Int) =
        convertDrawableToBitmap(AppCompatResources.getDrawable(context, resourceId))

    private fun convertDrawableToBitmap(sourceDrawable: Drawable?): Bitmap? {
        if (sourceDrawable == null) {
            return null
        }
        return if (sourceDrawable is BitmapDrawable) {
            sourceDrawable.bitmap
        } else {
            val constantState = sourceDrawable.constantState ?: return null
            val drawable = constantState.newDrawable().mutate()
            val bitmap: Bitmap = Bitmap.createBitmap(
                drawable.intrinsicWidth, drawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            bitmap
        }
    }

    override fun onBackPressed() {
        backAlert()
    }

    private fun backAlert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.exit_nav))
        builder.setMessage(getString(R.string.back_map))
        builder.setCancelable(false)
        builder.setPositiveButton(getString(R.string.yes)) { dialog, which ->
            super.onBackPressed()
        }
        builder.setNegativeButton(getString(R.string.no)) { dialog, which -> }
        builder.show()
    }

}

