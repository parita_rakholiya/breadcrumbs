package com.example.breadcrumbs.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.databinding.ActivityForgotPasswordBinding;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.CommonResponseModel;
import com.example.breadcrumbs.model.SignUpDetails;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.FacebookLogin;
import com.example.breadcrumbs.utils.PrefManager;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.Arrays;

import ir.shahabazimi.instagrampicker.PickerViewControll;
import ir.shahabazimi.instagrampicker.interfaces.dialogPositiveClickListener;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityForgotPasswordBinding forgotPasswordBinding;
    private Activity activity;
    private String email;
    private static ForgotPasswordActivity forgotPasswordActivity;
    private BottomSheetDialog bottomSheetDialog;
    private LoginButton loginButton;
    private CallbackManager callbackManager;

    public static ForgotPasswordActivity getInstance(){
        return forgotPasswordActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forgotPasswordBinding = DataBindingUtil.setContentView(this,
                R.layout.activity_forgot_password);
        activity = ForgotPasswordActivity.this;
        forgotPasswordActivity = this;
        initView();
    }

    private void initView() {
        facebookSdkInitialization();
        forgotPasswordBinding.imgBack.setOnClickListener(this);
        forgotPasswordBinding.btnNext.setOnClickListener(this);
        forgotPasswordBinding.imgFacebbok.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
        if(id==R.id.btn_next){
            if(checkValidation()){
                callForgotPasswordApi();
            }
        }
        if (id == R.id.img_facebbok) {

            loginButton.performClick();
        }
    }

    private void facebookSdkInitialization() {
        FacebookSdk.sdkInitialize(activity,new FacebookSdk.InitializeCallback(){
            @Override
            public void onInitialized() {

            }
        });
        loginButton = new LoginButton(activity);

        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email"));

        loginButton.setLoginBehavior(LoginBehavior.WEB_ONLY);

        // Creating CallbackManager
        callbackManager = CallbackManager.Factory.create();

        new FacebookLogin(loginButton,activity,callbackManager);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }

    private boolean checkValidation(){
        String email = forgotPasswordBinding.etEmail.getText().toString();
        if(TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(activity, getString(R.string.valid_email), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void callForgotPasswordApi(){
        email = forgotPasswordBinding.etEmail.getText().toString();
        Loader.show(activity);
        APIManager.forgotPasswordApi(email,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                CommonResponseModel commonResponseModel = (CommonResponseModel) modelclass;
                Loader.hide(activity);
                if(!commonResponseModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                bottomSheetDialog = openSentOtpToMailDailog();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private BottomSheetDialog openSentOtpToMailDailog(){
        return PickerViewControll.showBottomSheetDialog(activity, getString(R.string.otp_verify),
                getString(R.string.next), false, new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                        SignUpDetails signUpDetails = new SignUpDetails();
                        signUpDetails.setEmail(email);
                        startActivity(new Intent(activity,VerifyEmailOtpActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra(Constants.SIGNUP_DETAILS,signUpDetails)
                                .putExtra(Constants.IS_FROM,Constants.FORGOT_PASSWORD)
                                .putExtra(Constants.USER_ID,""));

                    }

                    @Override
                    public void negativeClick() {

                    }
                });
    }
}