package com.example.breadcrumbs.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.Toast;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.adapter.MyRoutesDataAdapter;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.databinding.ActivityAboutUsBinding;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.AboutUsModel;
import com.example.breadcrumbs.model.GetDistanceModel;
import com.example.breadcrumbs.model.GetMyRoutesModel;
import com.example.breadcrumbs.utils.PrefManager;

public class AboutUsActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityAboutUsBinding aboutUsBinding;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aboutUsBinding = DataBindingUtil.setContentView(this,R.layout.activity_about_us);
        activity =  AboutUsActivity.this;
        initView();
    }

    private void initView() {
        aboutUsBinding.imgBack.setOnClickListener(this);
        getAboutUsApi();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.img_back){
            onBackPressed();
        }
    }

    private void getAboutUsApi(){
        Loader.show(activity);
        APIManager.getAboutUs(new APICaller.APICallBack(){
            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                AboutUsModel aboutUsModel = (AboutUsModel) modelclass;
                Loader.hide(activity);
                Spanned htmlString = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    htmlString = Html.fromHtml(aboutUsModel.getContent(),Html.FROM_HTML_MODE_LEGACY);
                }else {
                    htmlString = Html.fromHtml(aboutUsModel.getContent());
                }
                aboutUsBinding.txtAboutUs.setText(htmlString);
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }
}