package com.example.breadcrumbs.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.databinding.ActivityLoginScreenBinding;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.SignupModel;
import com.example.breadcrumbs.model.GetUserProfileApi;
import com.example.breadcrumbs.model.LoginApiModel;
import com.example.breadcrumbs.model.SignUpDetails;
import com.example.breadcrumbs.model.UserRegistrationApi;
import com.example.breadcrumbs.utils.ClickableText;
import com.example.breadcrumbs.utils.FacebookLogin;
import com.example.breadcrumbs.utils.PrefManager;
import com.example.breadcrumbs.utils.ViewControll;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class LoginScreenActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "LoginScreenActivity";
    private ActivityLoginScreenBinding loginScreenBinding;
    private Activity activity;
    private LoginButton loginButton;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginScreenBinding = DataBindingUtil.setContentView(this,R.layout.activity_login_screen);
        activity = LoginScreenActivity.this;
        initView();
    }

    private void initView() {
        loginScreenBinding.btnLogin.setOnClickListener(this);
        loginScreenBinding.imgFacebbok.setOnClickListener(this);
        loginScreenBinding.dontHaveAccount.setOnClickListener(this);
        loginScreenBinding.txtForgotPassword.setOnClickListener(this);
        facebookSdkInitialization();
        ClickableText.setClickableString(activity, getString(R.string.don_t_have_an_account), getString(R.string.don_t_have_an_account_sign_up_txt),
                loginScreenBinding.dontHaveAccount, new ClickableText.textClickListener() {
                    @Override
                    public void textClicked() {
                        startActivity(new Intent(activity,SignUpActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                });
        printHashKey();
    }

    private void printHashKey() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }

    private boolean checkValidation(){
        String email = loginScreenBinding.etEmail.getText().toString().trim();
        String password = loginScreenBinding.etPassword.getText().toString().trim();
        if(TextUtils.isEmpty(email) || !validateEmailAddress(email)){
            Toast.makeText(activity, getString(R.string.valid_email), Toast.LENGTH_SHORT).show();
            return false;
        }else if(password.isEmpty()){
            Toast.makeText(activity, getString(R.string.enter_password), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private boolean validateEmailAddress(String emailAddress){
        Matcher matcher =  android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress);
        return matcher.matches();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.btn_login){
            if(checkValidation()){
                callLoginApi();
            }
        }
        if (id == R.id.img_facebbok) {
            loginButton.performClick();
        }
        if(id==R.id.dont_have_account){
            startActivity(new Intent(activity,SignUpActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
        if(id==R.id.txt_forgot_password){
            startActivity(new Intent(activity,ForgotPasswordActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
        
    }

    private void callLoginApi(){
        String email = loginScreenBinding.etEmail.getText().toString();
        String password = loginScreenBinding.etPassword.getText().toString();
        Loader.show(activity);
        APIManager.loginApi(email,password,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                LoginApiModel loginApiModel = (LoginApiModel) modelclass;
                if(!loginApiModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Loader.hide(activity);
                    Toast.makeText(activity, loginApiModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }

                String userActiveStatus = loginApiModel.getUser().getStatus();
                if(userActiveStatus.equals("0")){
                    Loader.hide(activity);
                    Toast.makeText(activity, activity.getString(R.string.user_locked), Toast.LENGTH_SHORT).show();
                    return null;
                }

                PrefManager.putString(APIConstant.KeyParameter.USER_ID,loginApiModel.getUser().getId());
                getLoginUserInfo(activity,loginApiModel.getMessage());
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private void getLoginUserInfo(Activity activity,String message){
        GetUserProfileApi.getLoginUserInfo(activity, message, new GetUserProfileApi.apiResponseListener() {
            @Override
            public void responseSuccess() {
                gotoHomeActivity();
            }
        });
    }

    private void gotoHomeActivity(){
            startActivity(new Intent(activity, HomeScreenActivity.class).
                    setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            |Intent.FLAG_ACTIVITY_CLEAR_TASK));
            finishAffinity();

    }

    private void facebookSdkInitialization() {
        FacebookSdk.sdkInitialize(activity,new FacebookSdk.InitializeCallback(){
            @Override
            public void onInitialized() {

            }
        });
        loginButton = new LoginButton(activity);

        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email"));

        loginButton.setLoginBehavior(LoginBehavior.WEB_ONLY);

        // Creating CallbackManager
        callbackManager = CallbackManager.Factory.create();

        new FacebookLogin(loginButton,activity,callbackManager);

    }


}