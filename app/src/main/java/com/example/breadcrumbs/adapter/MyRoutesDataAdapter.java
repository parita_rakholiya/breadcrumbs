package com.example.breadcrumbs.adapter;

import static com.example.breadcrumbs.activity.NavigationActivityKt.EXTRA_NAVIGATION_POINT;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.activity.NavigationActivity;
import com.example.breadcrumbs.activity.OpenMyRoutesActivity;
import com.example.breadcrumbs.activity.TurnByTurnExperienceActivity;
import com.example.breadcrumbs.databinding.ItemMyRouteDataBinding;
import com.example.breadcrumbs.model.GetMyRoutesModel;
import com.example.breadcrumbs.model.SaveRouteModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.ViewControll;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapbox.geojson.Point;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MyRoutesDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<GetMyRoutesModel.Data> saveRouteModelList;
    private Activity activity;

    public MyRoutesDataAdapter(Activity context,List<GetMyRoutesModel.Data> saveRouteModelList) {
        this.activity = context;
        this.saveRouteModelList = saveRouteModelList;

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMyRouteDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_my_route_data, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        GetMyRoutesModel.Data itemData = saveRouteModelList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        viewHolder.bindItem(itemData);

    }

    @Override
    public int getItemCount() {
        return saveRouteModelList.size();
    }

    public void updateData(List<GetMyRoutesModel.Data> data) {
        saveRouteModelList.clear();
        saveRouteModelList.addAll(data);
        notifyDataSetChanged();
    }


    class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        ItemMyRouteDataBinding binding;

       public RecyclerViewViewHolder(ItemMyRouteDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindItem(GetMyRoutesModel.Data itemData) {
            binding.txtRouteName.setText(itemData.getName());
            binding.txtDistance.setText(itemData.getDistance()+" Kms");
            binding.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<GetMyRoutesModel.Data> historyRouteList = ViewControll.loadHistoryRouteList(activity);
                    historyRouteList.add(itemData);
                    ViewControll.saveHistoryList(activity,historyRouteList);
                    Point point = Point.fromLngLat(72.63041084865525,22.99353595489563);
                    activity.startActivity(new Intent(activity, NavigationActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra(EXTRA_NAVIGATION_POINT,point)
                    .putExtra(Constants.ROUTE_DETAIL,itemData));
                }
            });
            binding.executePendingBindings();
        }
    }
}
