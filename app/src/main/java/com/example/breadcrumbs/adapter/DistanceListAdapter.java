package com.example.breadcrumbs.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.model.DistanceListModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.LanguagePrefManager;
import com.example.breadcrumbs.utils.PrefManager;

import java.util.ArrayList;

public class DistanceListAdapter extends ArrayAdapter<DistanceListModel>{

    private ArrayList<DistanceListModel> dataSet = new ArrayList<>();
    private Activity mContext;
    private boolean isFromSetting= false;

    // View lookup cache
    private static class ViewHolder {
        TextView txtDistance;
        ImageView imgSelected;
        LinearLayout llSelected;
        LinearLayout llTxtDistance;
    }

    public DistanceListAdapter(ArrayList<DistanceListModel> data, Activity context,boolean isFromSetting) {
        super(context, R.layout.txt_row, data);
        this.dataSet = data;
        this.mContext=context;
        this.isFromSetting=isFromSetting;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DistanceListModel distance = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.txt_row, parent, false);
            viewHolder.txtDistance = (TextView) convertView.findViewById(R.id.txt_row);
            viewHolder.imgSelected = (ImageView) convertView.findViewById(R.id.img_selected);
            viewHolder.llSelected = (LinearLayout) convertView.findViewById(R.id.ll_selected);
            viewHolder.llTxtDistance = (LinearLayout) convertView.findViewById(R.id.ll_txt_distance);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        viewHolder.txtDistance.setText(distance.getDistance() +" " +distance.getMeterKm());

        if(isFromSetting) {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                    viewHolder.llTxtDistance.getLayoutParams();
            params.weight = 1F;
            params.gravity = Gravity.CENTER;
            viewHolder.llTxtDistance.setLayoutParams(params);
            viewHolder.llTxtDistance.setGravity(Gravity.CENTER);
            viewHolder.llSelected.setVisibility(View.GONE);
        }else {
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)
                    viewHolder.llTxtDistance.getLayoutParams();
            params.weight = 0.56f;
            viewHolder.llTxtDistance.setLayoutParams(params);
        }
        String prevSelecteddistance = PrefManager.getString(Constants.PIN_DISTANCE,"0");
            if(distance.equals(prevSelecteddistance)) {
                viewHolder.txtDistance.setTextColor(mContext.getResources().getColor(R.color.skin_light));
                if (!isFromSetting) {
                    viewHolder.imgSelected.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.imgSelected.setVisibility(View.GONE);
                }
            }else {
                viewHolder.txtDistance.setTextColor(mContext.getResources().getColor(R.color.brown));
                viewHolder.imgSelected.setVisibility(View.GONE);

            }
        return result;
    }
}