package com.example.breadcrumbs.adapter;

import static com.example.breadcrumbs.activity.NavigationActivityKt.EXTRA_NAVIGATION_POINT;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.activity.NavigationActivity;
import com.example.breadcrumbs.activity.OpenMyRoutesActivity;
import com.example.breadcrumbs.activity.TurnByTurnExperienceActivity;
import com.example.breadcrumbs.databinding.ItemHistoryRouteDataBinding;
import com.example.breadcrumbs.databinding.ItemMyRouteDataBinding;
import com.example.breadcrumbs.model.GetMyRoutesModel;
import com.example.breadcrumbs.model.SaveRouteModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.ViewControll;
import com.mapbox.geojson.Point;

import java.util.List;

public class HistoryRoutesDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<GetMyRoutesModel.Data> historyRouteModelList;
    private Activity activity;

    public HistoryRoutesDataAdapter(Activity context, List<GetMyRoutesModel.Data> historyRouteModelList) {
        this.activity = context;
        this.historyRouteModelList = historyRouteModelList;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemHistoryRouteDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_history_route_data, parent, false);

        return new RecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        GetMyRoutesModel.Data itemData = historyRouteModelList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        viewHolder.bindItem(itemData);

    }

    @Override
    public int getItemCount() {
        return historyRouteModelList.size();
    }

    class RecyclerViewViewHolder extends RecyclerView.ViewHolder {

        ItemHistoryRouteDataBinding binding;

       public RecyclerViewViewHolder(ItemHistoryRouteDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindItem(GetMyRoutesModel.Data itemData) {
            binding.txtRouteName.setText(itemData.getName());
            String date = ViewControll.formateDateFromstring("dd-MMM-yyyy HH:mm:ss a",
                    "dd.MM.yyyy",itemData.getStartDate());
            binding.txtDate.setText(date);
            binding.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Point point = Point.fromLngLat(72.63041084865525,22.99353595489563);
                    activity.startActivity(new Intent(activity, NavigationActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .putExtra(EXTRA_NAVIGATION_POINT,point)
                            .putExtra(Constants.ROUTE_DETAIL,itemData));
                }
            });
            binding.executePendingBindings();
        }
    }
}
