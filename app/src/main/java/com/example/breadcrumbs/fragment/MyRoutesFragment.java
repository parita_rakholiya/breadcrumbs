package com.example.breadcrumbs.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.activity.CreateRouteActivity;
import com.example.breadcrumbs.activity.HomeScreenActivity;
import com.example.breadcrumbs.adapter.HistoryRoutesDataAdapter;
import com.example.breadcrumbs.adapter.MyRoutesDataAdapter;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.databinding.FragmentMyRoutesBinding;
import com.example.breadcrumbs.databinding.FragmentSettingBinding;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.CommonResponseModel;
import com.example.breadcrumbs.model.GetMyRoutesModel;
import com.example.breadcrumbs.model.SaveRouteModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.PrefManager;
import com.example.breadcrumbs.utils.ViewControll;

import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyRoutesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyRoutesFragment extends Fragment implements View.OnClickListener {

    private FragmentMyRoutesBinding myRoutesBinding;
    private Activity activity;
    private MyRoutesDataAdapter myRoutesDataAdapter;
    private HistoryRoutesDataAdapter historyRoutesDataAdapter;


    public MyRoutesFragment() {
        // Required empty public constructor
    }

    public static MyRoutesFragment newInstance() {
        MyRoutesFragment fragment = new MyRoutesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myRoutesBinding = FragmentMyRoutesBinding.inflate(inflater, container, false);
        activity = getActivity();
        return myRoutesBinding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myRoutesBinding.llCreateRoute.setOnClickListener(this);
        myRoutesBinding.llHistory.setOnClickListener(this);
        myRoutesBinding.recyclerMyRoutes.setNestedScrollingEnabled(false);
        myRoutesBinding.recyclerHistory.setNestedScrollingEnabled(false);


    }

    @Override
    public void onResume() {
        super.onResume();
        if(!Constants.IS_HISTORY_CLICK){
            getMyRoutesApi();
//            setMyRoutesItemList();
        }
    }

    private void setMyRoutesItemList(){
        myRoutesBinding.llMyRoutes.setVisibility(View.VISIBLE);
        myRoutesBinding.llHistoryLayout.setVisibility(View.GONE);
        if(ViewControll.loadSavedRouteList(getActivity()).size()!=0){
            List<GetMyRoutesModel.Data> routeList = ViewControll.loadSavedRouteList(getActivity());
            myRoutesDataAdapter = new MyRoutesDataAdapter(activity,routeList);
            myRoutesBinding.recyclerMyRoutes.setAdapter(myRoutesDataAdapter);
        }
    }

    private void setHistoryItemList(){
        if(ViewControll.loadHistoryRouteList(getActivity()).size()!=0){
            List<GetMyRoutesModel.Data> historyList = ViewControll.loadHistoryRouteList(getActivity());
            historyList = ViewControll.removeDuplicates(historyList);
            historyRoutesDataAdapter = new HistoryRoutesDataAdapter(activity,historyList);
            myRoutesBinding.recyclerHistory.setAdapter(historyRoutesDataAdapter);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.ll_create_route){
            startActivity(new Intent(activity, CreateRouteActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
        if(id == R.id.llHistory){
            if(ViewControll.loadHistoryRouteList(getActivity()).size()!=0){
                Constants.IS_HISTORY_CLICK = true;
                setHistoryItemList();
                ((HomeScreenActivity)activity).homeScreenBinding.imgBack.setVisibility(View.VISIBLE);
                ((HomeScreenActivity)activity).homeScreenBinding.txtTitle.setText(R.string.history);
                ((HomeScreenActivity)activity).homeScreenBinding.txtEdit.setVisibility(View.GONE);
                myRoutesBinding.llMyRoutes.setVisibility(View.GONE);
                myRoutesBinding.llHistoryLayout.setVisibility(View.VISIBLE);
            }else {
                Toast.makeText(activity, getString(R.string.history_not_found), Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void backFromHistory() {
        ((HomeScreenActivity)activity).homeScreenBinding.imgBack.setVisibility(View.GONE);
        ((HomeScreenActivity)activity).homeScreenBinding.txtTitle.setText(R.string.my_routes);
        myRoutesBinding.llMyRoutes.setVisibility(View.VISIBLE);
        myRoutesBinding.llHistoryLayout.setVisibility(View.GONE);
    }

    private void getMyRoutesApi(){
        String userId = PrefManager.getString(APIConstant.KeyParameter.USER_ID,"0");
        Loader.show(activity);
        APIManager.getMyRoutesList(userId,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                GetMyRoutesModel getMyRoutesModel = (GetMyRoutesModel) modelclass;
                Loader.hide(activity);
                if(!getMyRoutesModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Toast.makeText(activity, getMyRoutesModel.getMessage(), Toast.LENGTH_SHORT).show();
                    myRoutesBinding.llHistory.setVisibility(View.GONE);
                    return null;
                }

                if(myRoutesDataAdapter==null){
                    myRoutesBinding.llHistory.setVisibility(View.VISIBLE);
                    myRoutesDataAdapter = new MyRoutesDataAdapter(activity,getMyRoutesModel.getData());
                    myRoutesBinding.recyclerMyRoutes.setAdapter(myRoutesDataAdapter);
                }else {
                    myRoutesDataAdapter.updateData(getMyRoutesModel.getData());
                }



                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }
}