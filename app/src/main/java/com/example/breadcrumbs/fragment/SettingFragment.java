package com.example.breadcrumbs.fragment;

import static com.example.breadcrumbs.activity.HomeScreenActivity.setDeviceTokenAPi;
import static com.example.breadcrumbs.utils.PrefManager.getStringByLocal;
import static com.example.breadcrumbs.utils.ViewControll.openDistanceListDailog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Looper;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.activity.AboutUsActivity;
import com.example.breadcrumbs.activity.HomeScreenActivity;
import com.example.breadcrumbs.activity.LoginScreenActivity;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.databinding.FragmentMyProfileBinding;
import com.example.breadcrumbs.databinding.FragmentSettingBinding;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.CommonResponseModel;
import com.example.breadcrumbs.model.DistanceListWithId;
import com.example.breadcrumbs.model.GetDistanceModel;
import com.example.breadcrumbs.model.GetUserProfileApi;
import com.example.breadcrumbs.model.ProfileInfoModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.LanguagePrefManager;
import com.example.breadcrumbs.utils.PrefManager;
import com.example.breadcrumbs.utils.ViewControll;

import java.util.ArrayList;

import ir.shahabazimi.instagrampicker.interfaces.dialogPositiveClickListener;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingFragment extends Fragment implements View.OnClickListener {

    private FragmentSettingBinding settingBinding;
    private Activity activity;
    private String selectedDistance = "";
    private boolean isEnglishLanguageSelect = true;
    private boolean isUrduLanguageSelect = false;

    public SettingFragment() {
        // Required empty public constructor
    }

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        settingBinding = FragmentSettingBinding.inflate(inflater, container, false);
        activity = getActivity();
        return settingBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        settingBinding.llLogout.setOnClickListener(this);
        settingBinding.llPrivacyPolicy.setOnClickListener(this);
        settingBinding.llTermsCondition.setOnClickListener(this);
        settingBinding.txtDistance.setOnClickListener(this);
        settingBinding.switchCloseAccount.setOnClickListener(this);
        settingBinding.switchNotification.setOnClickListener(this);
        settingBinding.llNotify.setOnClickListener(this);
        settingBinding.llAboutUs.setOnClickListener(this);
        settingBinding.llCloseAccount.setOnClickListener(this);
        if(LanguagePrefManager.getString(Constants.SELECTED_LANGUAGE,"en").equals("en")){
            settingBinding.radioBtnEnglish.setChecked(true);
            settingBinding.radioBtnUrdu.setChecked(false);
        }else {
            settingBinding.radioBtnEnglish.setChecked(false);
            settingBinding.radioBtnUrdu.setChecked(true);
        }
        settingBinding.radioBtnEnglish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isEnglishLanguageSelect = isChecked;
                if (isChecked) {
                    ViewControll.setLocale(getActivity(),"en");
                    settingBinding.radioBtnEnglish.setChecked(true);
                    settingBinding.radioBtnUrdu.setChecked(false);
                    refreshApp();

                }
            }
        });
        settingBinding.radioBtnUrdu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isUrduLanguageSelect = isChecked;
                if (isChecked) {
                    ViewControll.setLocale(getActivity(),"ar");
                    settingBinding.radioBtnEnglish.setChecked(false);
                    settingBinding.radioBtnUrdu.setChecked(true);
                    refreshApp();
                }
            }
        });

    }

    private void refreshApp(){
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        String distanceID = PrefManager.getString(Constants.PIN_DISTANCE_ID,"0");
        String language = LanguagePrefManager.getString(Constants.SELECTED_LANGUAGE,"en");
        if(!distanceID.equals("0")) {
            APIManager.getSelectedDistance(distanceID, new APICaller.APICallBack() {

                @Override
                public <T> Class<T> onSuccess(T modelclass) {
                    GetDistanceModel getDistanceModel = (GetDistanceModel) modelclass;
                    if (!getDistanceModel.getStatusCode().equalsIgnoreCase("200 OK")) {
                        return null;
                    }

                    GetDistanceModel.Data data = getDistanceModel.getData().get(0);
                    if (language.equals("en")) {
                        selectedDistance = data.getDistance() + " " +
                                data.getMeterKm();
                    } else {
                        selectedDistance = data.getDistance() + " " +
                                data.getMeter_km_ar();
                    }

                    PrefManager.putString(Constants.PIN_DISTANCE, selectedDistance);
                    PrefManager.putString(Constants.PIN_DISTANCE_ID, data.getId());
                    progressDialog.dismiss();
                    progressDialog.cancel();
                    startActivity(new Intent(activity, HomeScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    Constants.REFRESH_APP = true;
                    activity.finishAffinity();

                    return null;
                }

                @Override
                public void onFailure() {
                    Loader.hide(activity);
                }
            });
        }else {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                    progressDialog.cancel();
                    startActivity(new Intent(activity, HomeScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    Constants.REFRESH_APP = true;
                    activity.finishAffinity();
                }
            },3000);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        String prevSelecteddistance = PrefManager.getString(Constants.PIN_DISTANCE,"0");
        if(!prevSelecteddistance.equals("0")){
            settingBinding.txtDistance.setText(prevSelecteddistance);
        }
        String status = PrefManager.getString(Constants.NOTIFICATION_STATUS,activity.getString(R.string._1));
        if(status.equals("1")) {
            settingBinding.switchNotification.setImageResource(R.drawable.ic_switch_on);
        }else{
            settingBinding.switchNotification.setImageResource(R.drawable.ic_switch_off);
        }
    }

    private void logout() {
        ViewControll.showAlertDialog(activity, getString(R.string.logout),
                getString(R.string.logout_msg), new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                       callLogoutApi();
                    }

                    @Override
                    public void negativeClick() {

                    }
                });

    }

    private void callLogoutApi(){
        Loader.show(activity);
        APIManager.logoutApi(new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                CommonResponseModel commonResponseModel = (CommonResponseModel) modelclass;
                Loader.hide(activity);
                if(!commonResponseModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                deleteUser();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private void closeAccount() {
        ViewControll.showAlertDialog(activity, getString(R.string.close_account),
                getString(R.string.close_account_msg), new dialogPositiveClickListener() {
                    @Override
                    public void positiveClick() {
                       closeAccountApi();
                    }

                    @Override
                    public void negativeClick() {
                        settingBinding.switchCloseAccount.setImageResource(R.drawable.ic_switch_off);
                    }
                });

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ll_logout) {
            logout();
        }
        if (id == R.id.txt_distance) {
            openDistanceListDailog(activity,true,
                    settingBinding.txtDistance,settingBinding.llMain,settingBinding.txtDistance);
        }
        if (id == R.id.ll_close_account) {
            settingBinding.switchCloseAccount.performClick();
            closeAccount();
        }
        if (id == R.id.ll_notify) {
            settingBinding.switchNotification.performClick();
        }
        if (id == R.id.ll_about_us) {
            startActivity(new Intent(activity, AboutUsActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
        if (id == R.id.ll_privacy_policy) {
            openPrivacyPolicy(Constants.PRIVACY_POLICY);
        }
        if (id == R.id.ll_terms_condition) {
            openPrivacyPolicy(Constants.TERMS_CONDITION);
        }
        if (id == R.id.switch_close_account) {
            if(settingBinding.switchCloseAccount.getDrawable().getConstantState() ==
                    getResources().getDrawable(R.drawable.ic_switch_off).getConstantState()){
                settingBinding.switchCloseAccount.setImageResource(R.drawable.ic_switch_on);
                closeAccount();
            }else {
                settingBinding.switchCloseAccount.setImageResource(R.drawable.ic_switch_off);
            }
        }
        if (id == R.id.switch_notification) {
            if(settingBinding.switchNotification.getDrawable().getConstantState() ==
                    getResources().getDrawable(R.drawable.ic_switch_off).getConstantState()) {
                settingBinding.switchNotification.setImageResource(R.drawable.ic_switch_on);
                PrefManager.putString(Constants.NOTIFICATION_STATUS,getString(R.string._1));
                setDeviceTokenAPi(activity);

            }else{
                settingBinding.switchNotification.setImageResource(R.drawable.ic_switch_off);
                PrefManager.putString(Constants.NOTIFICATION_STATUS,getString(R.string._0));
                setDeviceTokenAPi(activity);
            }
        }
    }

    private void openPrivacyPolicy(String privacy_link){
        try {
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(privacy_link));
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(myIntent);
        }catch (ActivityNotFoundException e) {
            Toast.makeText(activity, "No application can handle this request."
                    + " Please install a webbrowser",  Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    private void closeAccountApi(){
        Loader.show(activity);
        String userId = PrefManager.getString(APIConstant.KeyParameter.USER_ID,"0");
        APIManager.closeAccountApi(userId,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                CommonResponseModel commonResponseModel = (CommonResponseModel) modelclass;
                Loader.hide(activity);
                if(!commonResponseModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }

                Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                deleteUser();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }


    private void deleteUser(){
        PrefManager.deleteuser();
        startActivity(new Intent(activity, LoginScreenActivity.class).
                setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        activity.finishAffinity();
    }
}