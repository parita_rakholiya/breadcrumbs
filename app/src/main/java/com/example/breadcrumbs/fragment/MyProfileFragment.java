package com.example.breadcrumbs.fragment;

import static com.example.breadcrumbs.utils.ViewControll.checkPhoneNumberValidation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.activity.HomeScreenActivity;
import com.example.breadcrumbs.activity.LoginScreenActivity;
import com.example.breadcrumbs.activity.VerifyEmailOtpActivity;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.databinding.FragmentMyProfileBinding;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.CommonResponseModel;
import com.example.breadcrumbs.model.CountryData;
import com.example.breadcrumbs.model.GetUserProfileApi;
import com.example.breadcrumbs.model.SignUpDetails;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.LanguagePrefManager;
import com.example.breadcrumbs.utils.PrefManager;
import com.example.breadcrumbs.utils.ViewControll;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;

import ir.shahabazimi.instagrampicker.InstagramPicker;
import ir.shahabazimi.instagrampicker.interfaces.dialogPositiveClickListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener {

    private FragmentMyProfileBinding profileBinding;
    private Activity activity;
    private SignUpDetails signUpDetails;
    private String profileImagePath;
    private String email,firstName,lastName,phoneNo,address,gender,country;
    MultipartBody.Part profileImage;
    private List<CountryData> countryDataList = new ArrayList<>();
    private String selectedLanguage;


    public MyProfileFragment() {
        // Required empty public constructor
    }


    public static MyProfileFragment newInstance() {
        MyProfileFragment fragment = new MyProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        profileBinding = FragmentMyProfileBinding.inflate(inflater, container, false);
        activity = getActivity();
        return profileBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        selectedLanguage = LanguagePrefManager.getString(Constants.SELECTED_LANGUAGE,"en");
        countryDataList = ViewControll.getAllCountryInfo(activity);
        getUserProfileInfo();
        setToolbariconClick();
        profileBinding.etCountry.setOnClickListener(this);
        profileBinding.etGender.setOnClickListener(this);
        profileBinding.llSave.setOnClickListener(this);
        profileBinding.llUploadProfilePhoto.setOnClickListener(this);
        profileBinding.etCountry.setEnabled(false);
        profileBinding.etGender.setEnabled(false);
    }

    private void setToolbariconClick(){
        ((HomeScreenActivity)activity).homeScreenBinding.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.IS_EDIT_CLICK =  true;
                ViewControll.showKeyBoard(activity);
                ((HomeScreenActivity)activity).homeScreenBinding.imgBack.setVisibility(View.VISIBLE);
                ((HomeScreenActivity)activity).homeScreenBinding.txtTitle.setText(R.string.edit_profile);
                profileBinding.etFirstName.setFocusableInTouchMode(true);
                profileBinding.etFirstName.requestFocus();
                profileBinding.etLastName.setFocusableInTouchMode(true);
                profileBinding.etEmail.setFocusableInTouchMode(true);
                profileBinding.etPhone.setFocusableInTouchMode(true);
                profileBinding.etAddress.setFocusableInTouchMode(true);
                profileBinding.etCountry.setEnabled(true);
                profileBinding.etGender.setEnabled(true);
            }
        });

    }

    public void backFromEdit(){
        ((HomeScreenActivity)activity).homeScreenBinding.imgBack.setVisibility(View.GONE);
        profileBinding.llMain.requestFocus();
        ViewControll.hideKeyBoard(activity);
        ((HomeScreenActivity)activity).homeScreenBinding.txtTitle.setText(R.string.my_profile);
        profileBinding.etFirstName.setFocusable(false);
        profileBinding.etLastName.setFocusable(false);
        profileBinding.etEmail.setFocusable(false);
        profileBinding.etPhone.setFocusable(false);
        profileBinding.etAddress.setFocusable(false);
        profileBinding.etCountry.setEnabled(false);
        profileBinding.etGender.setEnabled(false);
    }

    private void getUserProfileInfo(){
        if(PrefManager.isUserExist()) {
            signUpDetails = PrefManager.loginUser();
            String email = signUpDetails.getEmail() != null ? signUpDetails.getEmail() : "";
            String firstName = signUpDetails.getFirstName() != null ? signUpDetails.getFirstName() : "";
            String lastName = signUpDetails.getLastName() != null ? signUpDetails.getLastName() : "";
            String phoneNo = signUpDetails.getPhoneNumber() != null ? signUpDetails.getPhoneNumber() : "";
            String address = signUpDetails.getAddress() != null ? signUpDetails.getAddress() : "";
            String gender = signUpDetails.getGender() != null ? signUpDetails.getGender() : "";
            String country = signUpDetails.getCountry() != null ? signUpDetails.getCountry() : "";

            profileBinding.etEmail.setText(email);
            profileBinding.etFirstName.setText(firstName);
            profileBinding.etLastName.setText(lastName);
            profileBinding.etAddress.setText(address);
            gender = PrefManager.checkForGenderLanguage(activity,gender,selectedLanguage);
            profileBinding.etGender.setText(gender);
            profileBinding.etCountry.setText(country);
            profileBinding.etPhone.setText(phoneNo);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id==R.id.et_gender){
            ViewControll.setGenderList(activity,profileBinding.etGender);
        }
        if(id==R.id.et_country){
            ViewControll.getCountryList(activity,profileBinding.etCountry);
        }
        if(id==R.id.ll_upload_profile_photo){
            if (ViewControll.isStoragePermissionGranted(activity)) {
                uploadProfilePicture();
            }
        }
        if(id==R.id.ll_save){
            if(checkValidation()){
                callEditProfileApi();
            }
        }
    }

    private void uploadProfilePicture(){
        InstagramPicker instagramPicker = new InstagramPicker(activity,true);
        //       this way for just a picture
        instagramPicker.show(1, 1, 1, address -> {
            //        receive image address in here
            Log.d("image==",address.get(0));
            profileImagePath = address.get(0);
            Toast.makeText(activity, getString(R.string.change_photo), Toast.LENGTH_SHORT).show();
        });
    }

    private void callEditProfileApi(){
        String userId = PrefManager.getString(APIConstant.KeyParameter.USER_ID,"0");
        HashMap<String, RequestBody> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.FIRST_NAME, ViewControll.convertStringToRequestBody(firstName));
        hashMap.put(APIConstant.Parameter.LAST_NAME,ViewControll.convertStringToRequestBody(lastName));
        hashMap.put(APIConstant.Parameter.EMAIL,ViewControll.convertStringToRequestBody(email));
        hashMap.put(APIConstant.Parameter.PHONE,ViewControll.convertStringToRequestBody(phoneNo));
        hashMap.put(APIConstant.Parameter.GENDER,ViewControll.convertStringToRequestBody(gender));
        hashMap.put(APIConstant.Parameter.COUNTRY,ViewControll.convertStringToRequestBody(country));
        hashMap.put(APIConstant.Parameter.ADDRESS,ViewControll.convertStringToRequestBody(address));
        hashMap.put(APIConstant.Parameter.ID,ViewControll.convertStringToRequestBody(userId));
        hashMap.put(APIConstant.Parameter.LANGUAGE,ViewControll.convertStringToRequestBody(ViewControll.getSelectedLanguage()));
        Loader.show(activity);
        if(profileImagePath!=null){
            // Parsing any Media type file
            RequestBody requestBody = RequestBody.create(new File(profileImagePath), MediaType.parse("image/*"));
            profileImage = MultipartBody.Part.createFormData("image",
                    new File(profileImagePath).getName(), requestBody);

        }
        APIManager.editProfileApi(hashMap,profileImage,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                CommonResponseModel commonResponseModel = (CommonResponseModel) modelclass;
                Loader.hide(activity);
                if(!commonResponseModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                profileImagePath = null;
                profileImage = null;
                    GetUserProfileApi.getLoginUserInfo(activity, commonResponseModel.getMessage(),
                            new GetUserProfileApi.apiResponseListener() {
                                @Override
                                public void responseSuccess() {
                                    getUserProfileInfo();
                                }
                            });

                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private boolean checkValidation(){
        email = profileBinding.etEmail.getText().toString().trim();
        address = profileBinding.etAddress.getText().toString().trim();
        firstName = profileBinding.etFirstName.getText().toString().trim();
        lastName = profileBinding.etLastName.getText().toString().trim();
        phoneNo = profileBinding.etPhone.getText().toString().trim();
        gender = profileBinding.etGender.getText().toString().trim();
        country = profileBinding.etCountry.getText().toString().trim();
        if(TextUtils.isEmpty(email) || !validateEmailAddress(email)){
            Toast.makeText(activity, getString(R.string.valid_email), Toast.LENGTH_SHORT).show();
            return false;
        }else if(firstName.isEmpty()){
            Toast.makeText(activity, getString(R.string.enter_first_name), Toast.LENGTH_SHORT).show();
            return false;
        }else if(lastName.isEmpty()){
            Toast.makeText(activity, getString(R.string.enter_last_name), Toast.LENGTH_SHORT).show();
            return false;
        }else if(address.isEmpty()){
            Toast.makeText(activity, getString(R.string.enter_address), Toast.LENGTH_SHORT).show();
            return false;
        }else if(gender.isEmpty()){
            Toast.makeText(activity, getString(R.string.select_gender), Toast.LENGTH_SHORT).show();
            return false;
        }else if(phoneNo.isEmpty() || !checkPhoneNumberValidation(countryDataList,country,phoneNo)){
            Toast.makeText(activity, getString(R.string.phoneno_alert), Toast.LENGTH_SHORT).show();
            return false;
        }else if(country.isEmpty()){
            Toast.makeText(activity, getString(R.string.select_country), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateEmailAddress(String emailAddress){
        Matcher matcher =  android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress);
        return matcher.matches();
    }
}