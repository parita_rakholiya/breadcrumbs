package com.example.breadcrumbs.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DistanceListModel {

    private String id;
    private String distance;
    private String meterKm;

    public DistanceListModel(String id, String distance, String meterKm) {
        this.id = id;
        this.distance = distance;
        this.meterKm = meterKm;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getMeterKm() {
        return meterKm;
    }

    public void setMeterKm(String meterKm) {
        this.meterKm = meterKm;
    }

}
