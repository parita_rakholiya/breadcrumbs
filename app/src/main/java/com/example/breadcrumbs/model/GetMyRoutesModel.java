package com.example.breadcrumbs.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "error",
        "status_code",
        "message"
})
public class GetMyRoutesModel implements Serializable {

    @JsonProperty("data")
    private List<Data> data = null;
    @JsonProperty("error")
    private Boolean error;
    @JsonProperty("status_code")
    private String statusCode;
    @JsonProperty("message")
    private String message;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("data")
    public List<Data> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Data> data) {
        this.data = data;
    }

    @JsonProperty("error")
    public Boolean getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(Boolean error) {
        this.error = error;
    }

    @JsonProperty("status_code")
    public String getStatusCode() {
        return statusCode;
    }

    @JsonProperty("status_code")
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "id",
            "name",
            "source",
            "destination",
            "distance",
            "startDate",
            "endDate",
            "routelist"
    })
    public static class Data implements Serializable{

        @JsonProperty("id")
        private String id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("source")
        private String source;
        @JsonProperty("destination")
        private String destination;
        @JsonProperty("startDate")
        private String startDate;
        @JsonProperty("endDate")
        private String endDate;
        @JsonProperty("routelist")
        private String routelist;
        @JsonProperty("distance")
        private String distance;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("id")
        public String getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(String id) {
            this.id = id;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("source")
        public String getSource() {
            return source;
        }

        @JsonProperty("source")
        public void setSource(String source) {
            this.source = source;
        }

        @JsonProperty("destination")
        public String getDestination() {
            return destination;
        }

        @JsonProperty("destination")
        public void setDestination(String destination) {
            this.destination = destination;
        }

        @JsonProperty("startDate")
        public String getStartDate() {
            return startDate;
        }

        @JsonProperty("startDate")
        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        @JsonProperty("endDate")
        public String getEndDate() {
            return endDate;
        }

        @JsonProperty("endDate")
        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        @JsonProperty("routelist")
        public String getRoutelist() {
            return routelist;
        }

        @JsonProperty("routelist")
        public void setRoutelist(String routelist) {
            this.routelist = routelist;
        }

        @JsonProperty("distance")
        public String getDistance() {
            return distance;
        }

        @JsonProperty("distance")
        public void setDistance(String distance) {
            this.distance = distance;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

}
