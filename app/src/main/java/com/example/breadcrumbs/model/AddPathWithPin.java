package com.example.breadcrumbs.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddPathWithPin implements Serializable {

    private boolean isPinedLocation;
    private HashMap<String,String> pathLatLng = new HashMap<>();

    public AddPathWithPin(HashMap<String, String> pathLatLng,boolean isPinedLocation) {
        this.pathLatLng = pathLatLng;
        this.isPinedLocation = isPinedLocation;
    }


    public boolean isPinedLocation() {
        return isPinedLocation;
    }

    public void setPinedLocation(boolean pinedLocation) {
        isPinedLocation = pinedLocation;
    }

    public HashMap<String, String> getPathLatLng() {
        return pathLatLng;
    }

    public void setPathLatLng(HashMap<String, String> pathLatLng) {
        this.pathLatLng = pathLatLng;
    }

}
