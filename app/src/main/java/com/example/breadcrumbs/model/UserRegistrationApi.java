package com.example.breadcrumbs.model;

import android.app.Activity;
import android.widget.Toast;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.utils.PrefManager;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class UserRegistrationApi {

    public static void userRegistrationAPI(Activity activity, HashMap<String, RequestBody> hashMap,String message,
                                           MultipartBody.Part image, apiResponseListener responseListener){
        Loader.show(activity);
        APIManager.verifyOtpApi(hashMap,image,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                SignupModel signupModel = (SignupModel) modelclass;
                if(!signupModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Loader.hide(activity);
                    Toast.makeText(activity, signupModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                PrefManager.putString(APIConstant.KeyParameter.USER_ID, String.valueOf(signupModel.getUser().get(0).getId()));
                GetUserProfileApi.getLoginUserInfo(activity, message, new GetUserProfileApi.apiResponseListener() {
                    @Override
                    public void responseSuccess() {
                        responseListener.responseSuccess();
                    }
                });
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    public static void fbUserRegistrationAPI(Activity activity, HashMap<String, String> hashMap,String message, apiResponseListener responseListener){
        Loader.show(activity);
        APIManager.facebookRegApi(hashMap,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                LockedUserModel signupModel = (LockedUserModel) modelclass;
                if(!signupModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Loader.hide(activity);
                    Toast.makeText(activity, signupModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                String userActiveStatus = signupModel.getUser().get(0).getStatus();
                if(userActiveStatus.equals("0")){
                    Loader.hide(activity);
                    Toast.makeText(activity, activity.getString(R.string.user_locked), Toast.LENGTH_SHORT).show();
                    return null;
                }

                PrefManager.putString(APIConstant.KeyParameter.USER_ID, String.valueOf(signupModel.getUser().get(0).getId()));
                GetUserProfileApi.getLoginUserInfo(activity, message, new GetUserProfileApi.apiResponseListener() {
                    @Override
                    public void responseSuccess() {
                        responseListener.responseSuccess();
                    }
                });
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    public interface apiResponseListener{
        void responseSuccess();
    }
}
