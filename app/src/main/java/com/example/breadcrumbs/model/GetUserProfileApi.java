package com.example.breadcrumbs.model;

import android.app.Activity;
import android.widget.Toast;

import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.utils.PrefManager;


public class GetUserProfileApi {

    public static void getLoginUserInfo(Activity activity, String message,apiResponseListener responseListener){
        String userId = PrefManager.getString(APIConstant.KeyParameter.USER_ID,"0");
        APIManager.getLoginUserInfo(userId,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                ProfileInfoModel profileInfoModel = (ProfileInfoModel) modelclass;
                Loader.hide(activity);

                if(!profileInfoModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Toast.makeText(activity, profileInfoModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }
                saveLoginUserData(profileInfoModel);
                if(!message.isEmpty()){
                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                }
                responseListener.responseSuccess();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private static void saveLoginUserData(ProfileInfoModel profileInfoModel){
        SignUpDetails signUpDetails = new SignUpDetails();
        ProfileInfoModel.Data data = profileInfoModel.getData().get(0);
        signUpDetails.setFirstName(data.getFirstName());
        signUpDetails.setLastName(data.getLastName());
        signUpDetails.setProfileImage(data.getImage());
        signUpDetails.setEmail(data.getEmail());
        signUpDetails.setCountry(data.getCountry());
        signUpDetails.setAddress(data.getAddress());
        signUpDetails.setGender(data.getGender());
        signUpDetails.setPhoneNumber(data.getPhone());
        signUpDetails.setFbProfileUrl(data.getProfileUrl());
        signUpDetails.setLoginType(data.getType());
        signUpDetails.setSocialId(data.getSocialId());
        PrefManager.saveLoginUser(signUpDetails);
    }

    public interface apiResponseListener{
        void responseSuccess();
    }
}
