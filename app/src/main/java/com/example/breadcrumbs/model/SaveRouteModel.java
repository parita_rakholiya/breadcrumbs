package com.example.breadcrumbs.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SaveRouteModel implements Serializable {

    private String categoryName,routeName;
    private double distance;
    private List<AddPathWithPin> pathRouteList = new ArrayList<>();

    public String getCategoryName() {
        return categoryName;
    }

    public SaveRouteModel(String categoryName, String routeName,
                          List<AddPathWithPin> pathRouteList,double distance) {
        this.categoryName = categoryName;
        this.routeName = routeName;
        this.pathRouteList = pathRouteList;
        this.distance = distance;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public List<AddPathWithPin> getPathRouteList() {
        return pathRouteList;
    }

    public void setPathRouteList(List<AddPathWithPin> pathRouteList) {
        this.pathRouteList = pathRouteList;
    }
    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }


}
