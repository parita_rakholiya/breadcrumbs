package com.example.breadcrumbs.model;

public class DistanceListWithId {
    private String name;
    private int id;

    public DistanceListWithId(String name,int id){

        this.id =id;
        this.name =name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
