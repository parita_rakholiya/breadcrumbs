package com.example.breadcrumbs.model;

import java.util.ArrayList;
import java.util.List;

public class CountryData {

    private String countryCode,countryName,CountryPhoneCode;
    private List<Integer> phoneNolength = new ArrayList<>();

    public CountryData(){}

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryPhoneCode() {
        return CountryPhoneCode;
    }

    public void setCountryPhoneCode(String countryPhoneCode) {
        CountryPhoneCode = countryPhoneCode;
    }

    public List<Integer> getPhoneNolength() {
        return phoneNolength;
    }

    public void setPhoneNolength(List<Integer> phoneNolength) {
        this.phoneNolength = phoneNolength;
    }


}
