package com.example.breadcrumbs.map;

import com.mapbox.navigation.ui.maps.camera.state.NavigationCameraState;

import kotlin.Metadata;

// $FF: synthetic class
@Metadata(
        mv = {1, 4, 3},
        bv = {1, 0, 3},
        k = 3
)
public class NavigationActivity$WhenMappings {
    public static final int[] $EnumSwitchMapping$0 = new int[NavigationCameraState.values().length];

    static {
        $EnumSwitchMapping$0[NavigationCameraState.TRANSITION_TO_FOLLOWING.ordinal()] = 1;
        $EnumSwitchMapping$0[NavigationCameraState.FOLLOWING.ordinal()] = 2;
        $EnumSwitchMapping$0[NavigationCameraState.TRANSITION_TO_OVERVIEW.ordinal()] = 3;
        $EnumSwitchMapping$0[NavigationCameraState.OVERVIEW.ordinal()] = 4;
        $EnumSwitchMapping$0[NavigationCameraState.IDLE.ordinal()] = 5;
    }

}
