package com.example.breadcrumbs.map;

import static com.example.breadcrumbs.activity.CreateRouteActivity.progressDialog;
import static com.example.breadcrumbs.service.MyFirebaseMessagingService.showNotification;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.animation.LinearInterpolator;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.example.breadcrumbs.R;
import com.example.breadcrumbs.activity.CreateRouteActivity;
import com.example.breadcrumbs.activity.ForgotPasswordActivity;
import com.example.breadcrumbs.activity.VerifyEmailOtpActivity;
import com.example.breadcrumbs.apiUtils.APICaller;
import com.example.breadcrumbs.apiUtils.APIConstant;
import com.example.breadcrumbs.apiUtils.APIManager;
import com.example.breadcrumbs.interfaces.saveRouteClickListener;
import com.example.breadcrumbs.loader.Loader;
import com.example.breadcrumbs.model.AddPathWithPin;
import com.example.breadcrumbs.model.CommonResponseModel;
import com.example.breadcrumbs.model.GetMyRoutesModel;
import com.example.breadcrumbs.model.SaveRouteModel;
import com.example.breadcrumbs.model.SendNotification;
import com.example.breadcrumbs.model.SignUpDetails;
import com.example.breadcrumbs.model.SignupModel;
import com.example.breadcrumbs.utils.Constants;
import com.example.breadcrumbs.utils.PrefManager;
import com.example.breadcrumbs.utils.ViewControll;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import ir.shahabazimi.instagrampicker.PickerViewControll;
import ir.shahabazimi.instagrampicker.interfaces.dialogPositiveClickListener;

public class GetCurrentLocation {

    private static final String TAG = "GetCurrentLocation";
    public static LatLng currentLocationLatlng = null ;
    public static LatLng pinLocationLatlng = null ;
    private static LatLng lastLocationLatlng = null ;
    private static LatLng startLocationLatlng = null ;
    private static Circle mCircle;
    private static Marker mMarker;
    private static Marker mCurrentPositionMarker;
    private static Marker mEndPositionMarker;
    private static GoogleMap mMap;
    private static Activity activity;
    private static LocationManager locationManager;
    private static LocationListener locationListener;
    private static LocationListener netWorkListner;
    private static List<AddPathWithPin> path = new ArrayList<>();
    private static LatLng previousLocation =null;
    private static Location currentLocation =null;
    public static PopupWindow saveRoutePopup;
    public static boolean isPathAdded =false;


    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    private static Boolean mRequestingLocationUpdates;

    /**
     * Time when the location was updated represented as a String.
     */
    private static String mLastUpdateTime;
  /**
     * Constant used in the location settings dialog.
     */
    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    private final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    private final static String KEY_LOCATION = "location";
    private final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";

    /**
     * Provides access to the Fused Location Provider API.
     */
    private static FusedLocationProviderClient mFusedLocationClient;

    /**
     * Provides access to the Location Settings API.
     */
    private static SettingsClient mSettingsClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private static LocationRequest mLocationRequest;

    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    private static LocationSettingsRequest mLocationSettingsRequest;

    /**
     * Callback for Location events.
     */
    private static LocationCallback mLocationCallback;

    /**
     * Represents a geographical location.
     */
    private static Location mCurrentLocation;


    public GetCurrentLocation(Activity activity, GoogleMap mMap){
        clearAllValue(true);
        this.activity = activity;
        this.mMap = mMap;
        getCurrentLocation(activity,mMap);

    }

    public GetCurrentLocation(Activity activity, GoogleMap mMap,List<AddPathWithPin> path){
        clearAllValue(true);
        this.activity = activity;
        this.mMap = mMap;
        DrawRoute.drawLiveLocationRouteWithPin(activity,resizeMarkerIcon(activity),path,mMap);
        drawMarkerAtPinedLocation(activity,path,mMap);

    }

    public static void getCurrentLocation(Activity activity, GoogleMap mMap) {
        Locale.setDefault(new Locale("en", "US"));
        /*int LOCATION_REFRESH_TIME = 1000; // 2 seconds to update
        int LOCATION_REFRESH_DISTANCE = 100; // 500 meters to update
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, false);
        locationListener = netWorkListner =  new LocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }

            @Override
            public void onLocationChanged(Location location) {
                currentLocation = location;
                double latitude = Double.parseDouble(new DecimalFormat("##.###").
                        format(location.getLatitude()));
                double longitude = Double.parseDouble(new DecimalFormat("##.###").
                        format(location.getLongitude()));
                LatLng currentLoc = new LatLng(latitude, longitude);
                if(Constants.IS_START_LOCATION_CLICK){
//                    if(startLocationLatlng!=null){
//                        location = getLocationInLatLngRad(500,location);
//                    }
//                    if(previousLocation==null){
//                        latitude = Double.parseDouble(new DecimalFormat("##.###").
//                                format(location.getLatitude()));
//                        longitude = Double.parseDouble(new DecimalFormat("##.###").
//                                format(location.getLongitude()));
//                        previousLocation = new LatLng(latitude,longitude);
//
//                    }else {
//                        latitude = Double.parseDouble(new DecimalFormat("##.###").
//                                format(previousLocation.latitude));
//                        latitude = latitude+0.001;
//                        longitude = Double.parseDouble(new DecimalFormat("##.###").
//                                format(previousLocation.longitude));
//                        longitude = longitude+0.001;
//                        previousLocation = new LatLng(latitude,longitude);
//                    }
//                    currentLoc = previousLocation;

                    startCreateRoute(currentLoc,false,false);

                    int selectedDistance = PrefManager.getInt(Constants.SELECTED_DISTANCE,0);
                    int calculatedDistance = calculationByDistance(pinLocationLatlng, currentLocationLatlng);
                    if(calculatedDistance>selectedDistance){
                        if(Constants.IS_DISTANCE_CHANGE){
                            pinLocationLatlng = currentLoc;
                            Constants.IS_DISTANCE_CHANGE = false;
                        }
                    }

                    if (selectedDistance==calculatedDistance){
                        if(selectedDistance!=0){
                            pinLocationLatlng = currentLoc;
                            addPinToCurrentLocation(mMap,currentLoc);
                        }
                    }

                }else if(Constants.IS_STOP_LOCATION_CLICK){
                    if(progressDialog!=null){
                        progressDialog.dismiss();
                        progressDialog.cancel();
                    }
                    stopRoute(lastLocationLatlng,mMap);
                }

                if(startLocationLatlng==null){
                    startLocationLatlng = currentLoc;
                    currentLocationLatlng = currentLoc;
                    pinLocationLatlng = currentLoc;
                    PrefManager.putString(Constants.START_LOCATION, String.valueOf(currentLoc));
                }else {
                    currentLocationLatlng = currentLoc;
                }

                if(!Constants.IS_START_LOCATION_CLICK) {
                    startCreateRoute(currentLoc, true,false);
                }

                if(mCircle == null || mMarker == null){
                    drawMarkerWithCircle(activity,currentLoc,mMap);
                }else{
                    if(isPathAdded){
                        updateMarkerWithCircle(currentLoc,mMap);
                    }
                }
            }
        };

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(provider, LOCATION_REFRESH_TIME,
                0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_REFRESH_TIME,
                0, netWorkListner);*/
        initGpsLocation(activity);
    }

    private static void initGpsLocation(Activity activity){
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        mSettingsClient = LocationServices.getSettingsClient(activity);

        // Kick off the process of building the LocationCallback, LocationRequest, and
        // LocationSettingsRequest objects.
        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();
        startLocationUpdates(activity);
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    private static void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Creates a callback for receiving location events.
     */
    private static void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                updateLocationUI(mCurrentLocation);
            }
        };
    }

    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    private static void startLocationUpdates(Activity activity) {
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(activity, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        updateLocationUI(mCurrentLocation);
                    }
                })
                .addOnFailureListener(activity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);
                                Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show();
                                mRequestingLocationUpdates = false;
                        }

                        updateLocationUI(mCurrentLocation);
                    }
                });
    }

    private static void updateLocationUI(Location location) {
        if(location==null){
            return;
        }
        currentLocation = location;
        double latitude = Double.parseDouble(new DecimalFormat("##.###").
                format(location.getLatitude()));
        double longitude = Double.parseDouble(new DecimalFormat("##.###").
                format(location.getLongitude()));
        Log.d("update===",longitude + "---"+latitude);
        LatLng currentLoc = new LatLng(latitude, longitude);
        if(Constants.IS_START_LOCATION_CLICK){
//                    if(startLocationLatlng!=null){
//                        location = getLocationInLatLngRad(200,location);
//                    }
//                    if(previousLocation==null){
//                        latitude = Double.parseDouble(new DecimalFormat("##.###").
//                                format(location.getLatitude()));
//                        longitude = Double.parseDouble(new DecimalFormat("##.###").
//                                format(location.getLongitude()));
//                        previousLocation = new LatLng(latitude,longitude);
//
//                    }else {
//                        latitude = Double.parseDouble(new DecimalFormat("##.###").
//                                format(previousLocation.latitude));
//                        latitude = latitude+0.001;
//                        longitude = Double.parseDouble(new DecimalFormat("##.###").
//                                format(previousLocation.longitude));
//                        longitude = longitude+0.001;
//                        previousLocation = new LatLng(latitude,longitude);
//                    }
//                    currentLoc = previousLocation;
//                    currentLoc = new LatLng(location.getLatitude(),location.getLongitude());

            startCreateRoute(currentLoc,false,false);

            int selectedDistance = PrefManager.getInt(Constants.SELECTED_DISTANCE,0);
            String selectedDistanceType = PrefManager.getString(Constants.DISTNACE_TYPE,"");
            int calculatedDistance = calculationByDistance(
                    pinLocationLatlng, currentLocationLatlng,selectedDistanceType);
            if(calculatedDistance>selectedDistance){
                if(Constants.IS_DISTANCE_CHANGE){
                    pinLocationLatlng = currentLoc;
                    Constants.IS_DISTANCE_CHANGE = false;
                }
            }

            if (selectedDistance==calculatedDistance){
                if(selectedDistance!=0){
                    pinLocationLatlng = currentLoc;
                    addPinToCurrentLocation(mMap,currentLoc);
                }
            }

        }else if(Constants.IS_STOP_LOCATION_CLICK){
            if(progressDialog!=null){
                progressDialog.dismiss();
                progressDialog.cancel();
            }
            stopRoute(lastLocationLatlng,mMap);
        }

        if(startLocationLatlng==null){
            startLocationLatlng = currentLoc;
            currentLocationLatlng = currentLoc;
            pinLocationLatlng = currentLoc;
            PrefManager.putString(Constants.START_LOCATION, String.valueOf(currentLoc));
        }else {
            currentLocationLatlng = currentLoc;
        }

        if(!Constants.IS_START_LOCATION_CLICK) {
            startCreateRoute(currentLoc, true,false);
        }

        if(mCircle == null || mMarker == null){
            drawMarkerWithCircle(activity,currentLoc,mMap);
        }else{
            if(isPathAdded){
                updateMarkerWithCircle(currentLoc,mMap);
            }
        }

    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    private static void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }


    public static void addPinToCurrentLocation(GoogleMap googleMap,LatLng currentLocation){
        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("lat", Double.toString(currentLocation.latitude));
        hm.put("lng", Double.toString(currentLocation.longitude));
        path.add(new AddPathWithPin(hm,true));
        Marker pinMarker = googleMap.addMarker(new MarkerOptions()
                .position(currentLocation)
                .title("Marker in PinLocation")
                .icon(resizeMarkerIcon(activity)));
    }

    public static void startCreateRoute(LatLng currentLoc, boolean isStartLocation, boolean isPinedLocation){
        isPathAdded = false;
        if(startLocationLatlng!=null && currentLocationLatlng!=null){
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("lat", Double.toString(currentLoc.latitude));
            hm.put("lng", Double.toString(currentLoc.longitude));
            if (lastLocationLatlng==null || !lastLocationLatlng.equals(currentLoc)) {
                if(!checkIfLocationExists(currentLoc,path)){
                    lastLocationLatlng = currentLoc;
                    path.add(new AddPathWithPin(hm,isPinedLocation));
                    isPathAdded = true;
                }
            }
            lastLocationLatlng = currentLoc;
            if(!isStartLocation){
                DrawRoute.drawLiveLocationRoute(activity,path,mMap);
                Log.d(TAG, "Current location:= Latitude:= " + currentLoc.latitude +
                        " Longitude:= " + currentLoc.longitude);
                if(mCurrentPositionMarker==null){
                    mCurrentPositionMarker = mMap.addMarker(new MarkerOptions()
                            .position(currentLoc)
                            .title("Marker in CurrentLocation"));
                }
            }
        }
    }

    private static void updateMarkerWithCircle(LatLng position,GoogleMap googleMap) {
        mMarker.setPosition(startLocationLatlng);
        if(mCurrentPositionMarker!=null){
//            mCurrentPositionMarker.setPosition(position);
            changePositionSmoothly(mCurrentPositionMarker,position);
        }
        if(Constants.IS_START_LOCATION_CLICK){
            mCircle.setCenter(position);
        }else if(Constants.IS_STOP_LOCATION_CLICK){
            mCircle.remove();
        }else {
            mCircle.setCenter(startLocationLatlng);
        }

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position,16));

    }

   private  static void changePositionSmoothly(Marker marker, LatLng newLatLng) {
       if (marker != null) {
           LatLng startPosition = marker.getPosition();
           LatLng endPosition = new LatLng(newLatLng.latitude, newLatLng.longitude);

           float startRotation = marker.getRotation();

           LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
           ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
           valueAnimator.setDuration(1000); // duration 1 second
           valueAnimator.setInterpolator(new LinearInterpolator());
           valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
               @Override public void onAnimationUpdate(ValueAnimator animation) {
                   try {
                       float v = animation.getAnimatedFraction();
                       LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                       marker.setPosition(newPosition);
                       marker.setRotation(computeRotation(v, startRotation, 0));
                   } catch (Exception ex) {
                       // I don't care atm..
                   }
               }
           });

           valueAnimator.start();
       }
    }

    private static float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }

    private static boolean checkIfLocationExists(LatLng latLng, List<AddPathWithPin> path){
        boolean isLocationExists = false;
        for(int i=0;i<path.size();i++){
            AddPathWithPin addPathWithPin = path.get(i);
            HashMap<String,String> point = addPathWithPin.getPathLatLng();

            double lat = Double.parseDouble(point.get("lat"));
            double lng = Double.parseDouble(point.get("lng"));
            LatLng position = new LatLng(lat, lng);
            if(position.latitude==latLng.latitude || position.longitude==latLng.longitude){
                isLocationExists = true;
                break;
            }
        }
        return isLocationExists;
    }

    private static void drawMarkerWithCircle(Activity activity,LatLng currentLoc,GoogleMap mGoogleMap){
        double radiusInMeters = 80;
        int strokeColor = activity.getResources().getColor(R.color.orange_light);
        int shadeColor = activity.getResources().getColor(R.color.orange_light_transparent);
        CircleOptions circleOptions = new CircleOptions().center(startLocationLatlng).radius(radiusInMeters).
                fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(5);
        mCircle = mGoogleMap.addCircle(circleOptions);
        mMarker = mGoogleMap.addMarker(new MarkerOptions()
                .position(startLocationLatlng)
                .title("Marker in StartLocation")
                .icon(resizeMarkerIcon(activity)));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startLocationLatlng,16));

    }

    private static void drawMarkerAtPinedLocation(Activity activity,List<AddPathWithPin> path,
                                                  GoogleMap mGoogleMap){
            LatLng startLocationLatlng = null;
            LatLng endLocationLatlng = null;
            for(int i=0;i<path.size();i++){
                if(i==0){
                    AddPathWithPin addPathWithPin = path.get(i);
                    HashMap<String,String> point = addPathWithPin.getPathLatLng();

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    startLocationLatlng = new LatLng(lat, lng);
                }
                if(i==path.size()-1){

                     AddPathWithPin addPathWithPin = path.get(i);
                    HashMap<String,String> point = addPathWithPin.getPathLatLng();

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    endLocationLatlng = new LatLng(lat, lng);
                }
            }
        mGoogleMap.addMarker(new MarkerOptions()
                .position(startLocationLatlng)
                .title("Marker in StartLocation")
                .icon(resizeMarkerIcon(activity)));
        mGoogleMap.addMarker(new MarkerOptions()
                .position(endLocationLatlng)
                .title("Marker in StartLocation")
                .icon(resizeMarkerIcon(activity)));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startLocationLatlng,16));

    }

    private static BitmapDescriptor resizeMarkerIcon(Activity activity){
        int height = 100;
        int width = 100;
        Bitmap b = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_pin_white);
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);
        return smallMarkerIcon;
    }

    public static int calculationByDistance(LatLng StartP, LatLng EndP,String distanceType) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.d("CurrentRadius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);
        if(distanceType.equalsIgnoreCase("km")){
            return kmInDec;
        }else {
            return meterInDec;
        }
    }

    public static Bitmap drawCircle(Activity activity,int width,int height, int borderWidth) {
        Bitmap canvasBitmap = Bitmap.createBitmap( width, height, Bitmap.Config.ARGB_8888);
        BitmapShader shader = new BitmapShader(canvasBitmap, Shader.TileMode.CLAMP,
                Shader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(shader);
        paint.setShader(null);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(activity.getResources().getColor(R.color.orange_light_transparent));
        paint.setStrokeWidth(borderWidth);
        Canvas canvas = new Canvas(canvasBitmap);
        float radius = width > height ? ((float) height) / 2f : ((float) width) / 2f;
        canvas.drawCircle(width / 2, height / 2, radius - borderWidth / 2, paint);
        return canvasBitmap;
    }

    protected static Location getLocationInLatLngRad(double radiusInMeters, Location location) {

        double y0 = location.getLatitude();
        double x0 = location.getLongitude();
        Random random = new Random();

        // Convert radius from meters to degrees.
        double radiusInDegrees = radiusInMeters / 111320f;

        // Get a random distance and a random angle.
        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        // Get the x and y delta values.
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);

        // Compensate the x value.
        double new_x = x / Math.cos(Math.toRadians(y0));

        double foundLatitude;
        double foundLongitude;

        foundLatitude = y0 + y;
        foundLongitude = x0 + new_x;

        Location copy = new Location("");
        copy.setLatitude(foundLatitude);
        copy.setLongitude(foundLongitude);
        Log.d("c","Rendom location:= Latitude:= " + foundLatitude +
                " Longitude:= " + foundLatitude);
        return copy;
    }


    public static void clearRoute()
    {
        removeLocationListener();
        clearAllValue(true);
    }

    public static void removeLocationListener()
    {
        if(mFusedLocationClient==null || locationManager==null){
            return;
        }
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;
                    }
                });
        locationManager.removeUpdates(locationListener);
        locationManager.removeUpdates(netWorkListner);
    }

    public static void stopRoute(LatLng latLng,GoogleMap mMap)
    {
        removeLocationListener();
        if(mCurrentPositionMarker!=null){
            mCurrentPositionMarker.remove();
        }
        mEndPositionMarker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Marker in DestinationLocation")
                .icon(resizeMarkerIcon(activity)));
        updateMarkerWithCircle(latLng,mMap);
        clearAllValue(false);
        saveRoutePopup = ViewControll.openSaveRouteDialog(activity, path,new saveRouteClickListener() {

            @Override
            public void positiveClick(String category, String routeName, String message, String sourceName, String destinationName) {
                SaveRouteModel saveRouteModel = new SaveRouteModel(category,routeName,path,
                        checkDistanceBtroute());
                String json = new Gson().toJson(saveRouteModel);
                callSaveRouteApi(activity,routeName,category,destinationName,
                        sourceName,json,message,String.valueOf(checkDistanceBtroute()));
            }

            @Override
            public void negativeClick(PopupWindow popupWindow) {
                ViewControll.showAlertDialog(activity, activity.getString(R.string.save_route),
                        activity.getString(R.string.not_save_route), new dialogPositiveClickListener() {
                            @Override
                            public void positiveClick() {
                                popupWindow.dismiss();
                                activity.finish();
                            }

                            @Override
                            public void negativeClick() {

                            }
                        });
            }
        });
    }

    private static void callSaveRouteApi(Activity activity,String name,String category,String destination,
                                  String source,String routelist,String message,String distance){
        String userId = PrefManager.getString(APIConstant.KeyParameter.USER_ID,"0");
        String prevSelecteddistance = PrefManager.getString(Constants.PIN_DISTANCE,"0");
        String prevSelecteddistanceID = PrefManager.getString(Constants.PIN_DISTANCE_ID,"0");
        String token = PrefManager.getString(Constants.FIREBASE_TOKEN,"");
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIConstant.Parameter.NAME,name);
        hashMap.put(APIConstant.Parameter.USER_ID,userId);
        hashMap.put(APIConstant.Parameter.CATEGORY,category);
        hashMap.put(APIConstant.Parameter.SOURCE,source);
        hashMap.put(APIConstant.Parameter.METER_KM,prevSelecteddistance);
        hashMap.put(APIConstant.Parameter.DS_ID,prevSelecteddistanceID);
        hashMap.put(APIConstant.Parameter.DESTINATION,destination);
        hashMap.put(APIConstant.Parameter.DISTANCE,distance);
        hashMap.put(APIConstant.Parameter.ROUTE_LIST,routelist);
        hashMap.put(APIConstant.Parameter.TOKEN,token);
        hashMap.put(APIConstant.Parameter.START_DATE,Constants.ROUTE_START_TIME);
        hashMap.put(APIConstant.Parameter.END_DATE,Constants.ROUTE_END_TIME);
        hashMap.put(APIConstant.Parameter.LANGUAGE, ViewControll.getSelectedLanguage());
        Loader.show(activity);

        APIManager.saveNewRouteAPI(hashMap,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                SignupModel commonResponseModel = (SignupModel) modelclass;
                if(!commonResponseModel.getStatusCode().equalsIgnoreCase("200 OK")){
                    Loader.hide(activity);
                    Toast.makeText(activity, commonResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    return null;
                }

                PrefManager.putString(Constants.ROUTE_ID,
                        String.valueOf(commonResponseModel.getUser().get(0).getId()));
                sendNotificationAPI(activity,activity.getString(R.string.app_name),message);
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private static void sendNotificationAPI(Activity activity,String title,String message){
        APIManager.sendNotification(title,message,new APICaller.APICallBack(){

            @Override
            public <T> Class<T> onSuccess(T modelclass) {
                SendNotification sendNotification = (SendNotification) modelclass;
                Loader.hide(activity);
                Constants.NOTIFICATION_TITLE = title;
                Constants.NOTIFICATION_MESSAGE = message;
                activity.finish();
                return null;
            }

            @Override
            public void onFailure() {
                Loader.hide(activity);
            }
        });
    }

    private static double checkDistanceBtroute(){
        LatLng startLocationLatlng = null;
        LatLng endLocationLatlng = null;
        for(int i=0;i<path.size();i++){
            if(i==0){
                AddPathWithPin addPathWithPin = path.get(i);
                HashMap<String,String> point = addPathWithPin.getPathLatLng();

                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                startLocationLatlng = new LatLng(lat, lng);
            }
            if(i==path.size()-1){
                AddPathWithPin addPathWithPin = path.get(i);
                HashMap<String,String> point = addPathWithPin.getPathLatLng();

                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                endLocationLatlng = new LatLng(lat, lng);
            }
        }
        String selectedDistanceType = PrefManager.getString(Constants.DISTNACE_TYPE,"");
        double distance = GetCurrentLocation.calculationByDistance(
                startLocationLatlng,endLocationLatlng,selectedDistanceType);
        return distance;
    }

    public static void  clearAllValue(boolean isMapClear){
        if(isMapClear){
            path.clear();
            if(mMap!=null){
                mMap.clear();
            }
        }
        Constants.IS_START_LOCATION_CLICK =false;
        Constants.IS_STOP_LOCATION_CLICK =false;
        Constants.IS_DISTANCE_CHANGE =false;
        mCircle = null;
        mMarker = null;
        mCurrentPositionMarker = null;
        currentLocationLatlng = null;
        startLocationLatlng = null;
        lastLocationLatlng = null;
        previousLocation = null;
    }

}
