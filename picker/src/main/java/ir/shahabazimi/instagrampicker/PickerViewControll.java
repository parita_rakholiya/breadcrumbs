package ir.shahabazimi.instagrampicker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import ir.shahabazimi.instagrampicker.interfaces.dialogPositiveClickListener;

public class PickerViewControll {

    public static BottomSheetDialog showBottomSheetDialog(Activity activity, String txt_msg, String btnText,boolean isCancelable,
                                                          dialogPositiveClickListener positiveClickListener) {

        final BottomSheetDialog bottomSheetDialog =
                new BottomSheetDialog(activity,R.style.CustomBottomSheetDialog);
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_profile_photo_uploaded);
        Button btnContinue = bottomSheetDialog.findViewById(R.id.btn_continue);
        TextView txtMsg = bottomSheetDialog.findViewById(R.id.txt_msg);
        txtMsg.setText(txt_msg);
        btnContinue.setText(btnText);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                positiveClickListener.positiveClick();
            }
        });
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(isCancelable);
        bottomSheetDialog.show();
        return bottomSheetDialog;
    }


    public static void showAlertDialog(Activity activity, String title, String message, dialogPositiveClickListener dialogPositiveClickListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.CustomAlertDialog);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogPositiveClickListener.positiveClick();
                dialog.dismiss();

            }
        });
        builder.setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
