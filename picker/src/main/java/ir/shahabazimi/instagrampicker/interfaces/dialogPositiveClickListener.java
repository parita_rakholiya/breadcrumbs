package ir.shahabazimi.instagrampicker.interfaces;

public interface dialogPositiveClickListener {

    void positiveClick();
    void negativeClick();
}
